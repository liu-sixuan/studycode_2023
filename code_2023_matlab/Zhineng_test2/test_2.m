Image_f = imread('red.jpg');
subplot(1,2,1); %将图窗划分为1*2的网格，在第一块位置创建坐标区
imshow(Image_f);
title('刘斯旋-19221108-信号灯RGB图像');

%1.RGB信号灯图像转换成HSV图像
hsv_f = rgb2hsv(Image_f);
H = hsv_f(:,:,1)*255;%提取H(色度)
S = hsv_f(:,:,2)*255;%提取S(饱和度)
V = hsv_f(:,:,3)*255;%提取V(亮度)
subplot(1,2,2);%在第二块位置创建坐标区
imshow(hsv_f);
title('刘斯旋-19221108-信号灯hsv图像');

%2.统计并输出HSV图像中绿色像素点个数
[y,x,z] = size(Image_f);    %原始图像尺寸
Green_y = zeros(y,1);   %绿色像素点统计赋初值0矩阵
for i = 1:y
    for j = 1:x
        if(((H(i,j)>=66)&&(H(i,j)<130)&&(V(i,j)>50)&&S(i,j)>30))%判断绿色像素点条件
            Green_y(i,1) = Green_y(i,1)+1;
        end
    end
end
Max_Green_y = max(Green_y);  %最大绿色像素点赋值--结果：336

%3.判断信号灯颜色并输出检测结果
if(Max_Green_y>300) %判断图像颜色
    Result = 1;
else
    Result = 2;
end
if(Result == 1)
    disp('检测结果为绿灯');
else
    disp('检测失败');
end



%4. 输出图像最大红色、绿色、黄色像素点，并比较三者的值，
% % 在命令行窗口输出信号灯颜色的判断结果。

Red_y = zeros(y, 1); % 红色像素点统计赋初值0矩阵
for i = 1:y
    for j = 1:x
        if ((H(i, j) <= 10 || H(i, j) >= 175) && V(i, j) > 50 && S(i, j) > 30) % 判断红色像素点条件
            Red_y(i, 1) = Red_y(i, 1) + 1;
        end
    end
end
Max_Red_y = max(Red_y); % 最大红色像素点赋值

% 5. 统计并输出HSV图像中黄色像素点个数
Yellow_y = zeros(y, 1); % 黄色像素点统计赋初值0矩阵
for i = 1:y
    for j = 1:x
        if ((H(i, j) >= 30 && H(i, j) <= 50) && V(i, j) > 50 && S(i, j) > 30) % 判断黄色像素点条件
            Yellow_y(i, 1) = Yellow_y(i, 1) + 1;
        end
    end
end
Max_Yellow_y = max(Yellow_y); % 最大黄色像素点赋值

% 比较三者的值并输出结果
disp(['最大红色像素点: ', num2str(Max_Red_y)]);
disp(['最大绿色像素点: ', num2str(Max_Green_y)]);
disp(['最大黄色像素点: ', num2str(Max_Yellow_y)]);

% 判断信号灯颜色并输出检测结果
if (Max_Green_y > Max_Red_y && Max_Green_y > Max_Yellow_y) % 判断图像颜色
    disp('检测结果为绿灯');
end
if (Max_Red_y > Max_Green_y && Max_Red_y > Max_Yellow_y)
    disp('检测结果为红灯');
end
if (Max_Yellow_y > Max_Green_y && Max_Yellow_y > Max_Red_y)
    disp('检测结果为黄灯');
end
