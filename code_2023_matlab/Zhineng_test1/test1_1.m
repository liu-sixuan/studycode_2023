A = imread('initial.PNG');  
imshow(A);
title('刘斯旋-19221108-initial');

bevSensor = load('birdsEyeConfig'); %加载鸟瞰图配置
%1.原始图转换鸟瞰图
birdsEyeImage = transformImage (bevSensor.birdsEyeConfig,A);
imshow(birdsEyeImage);
title('刘斯旋-19221108-鸟瞰图');

%2.鸟瞰图转换灰度图
width = 0.25;   %车道宽度赋值
birdsEyeBw = segmentLaneMarkerRidge(rgb2gray(birdsEyeImage), ...
    bevSensor.birdsEyeConfig,width);
imshow(birdsEyeBw);
title('刘斯旋-19221108-灰度图');

%3.鸟瞰原图上显示出检测的车道线
[imageX,imageY] = find(birdsEyeBw); %查找图像边界点
%将鸟瞰图坐标转化为车辆坐标
xyBoundPoints = imageToVehicle(bevSensor.birdsEyeConfig,[imageY,imageX]);
%使用抛物线模型查找车道线边界
boundaries = findParabolicLaneBoundaries(xyBoundPoints,width);
Xpoints = 3:30;%设置x点范围，表示在本车传感器范围内的车道点
%在图像上叠加车道，插入左车道
LanesBEI = insertLaneBoundary(birdsEyeImage,boundaries(1),bevSensor.birdsEyeConfig,Xpoints);
%插入右车道
LanesBEI = insertLaneBoundary(LanesBEI,boundaries(2),bevSensor.birdsEyeConfig,Xpoints,'Color','r');
imshow(LanesBEI);
title('刘斯旋-19221108-鸟瞰车道检测图');

%4.在原始RGB图像上显示车道检测结果
Xpoints = round(Xpoints * bevSensor.birdsEyeConfig.scaleX) + bevSensor.birdsEyeConfig.offsetX; %将x点转换为原始图像坐标系下的点
Ypoints = round(bevSensor.birdsEyeConfig.worldYExtent - boundaries(1).leftBoundary(Xpoints) * bevSensor.birdsEyeConfig.scaleY) + bevSensor.birdsEyeConfig.offsetY; %左车道线在原始图像坐标系下的y坐标
LanesRGB = insertShape(A, 'Line', [Xpoints', Ypoints'], 'Color', 'g', 'LineWidth', 2); %插入左车道
Ypoints = round(bevSensor.birdsEyeConfig.worldYExtent - boundaries(1).rightBoundary(Xpoints) * bevSensor.birdsEyeConfig.scaleY) + bevSensor.birdsEyeConfig.offsetY; %右车道线在原始图像坐标系下的y坐标
LanesRGB = insertShape(LanesRGB, 'Line', [Xpoints', Ypoints'], 'Color', 'r', 'LineWidth', 2); %插入右车道
imshow(LanesRGB);
title('刘斯旋-19221108-原始RGB图像车道检测结果');

% % 4. 在原始RGB图像上显示车道检测结果
% bevSensor = load('birdsEyeConfig'); %加载鸟瞰图配置
% Xpoints = round(Xpoints * bevSensor.birdsEyeConfig.scaleX) + bevSensor.birdsEyeConfig.offsetX; % 将x点转换为原始图像坐标系下的点
% Ypoints = round(bevSensor.birdsEyeConfig.worldYExtent - boundaries(1).leftBoundary(Xpoints) * bevSensor.birdsEyeConfig.scaleY) + bevSensor.birdsEyeConfig.offsetY; % 左车道线在原始图像坐标系下的y坐标
% LanesRGB = insertShape(A, 'Line', [Xpoints', Ypoints'], 'Color', 'g', 'LineWidth', 2); % 插入左车道
% Ypoints = round(bevSensor.birdsEyeConfig.worldYExtent - boundaries(1).rightBoundary(Xpoints) * bevSensor.birdsEyeConfig.scaleY) + bevSensor.birdsEyeConfig.offsetY; % 右车道线在原始图像坐标系下的y坐标
% LanesRGB = insertShape(LanesRGB, 'Line', [Xpoints', Ypoints'], 'Color', 'r', 'LineWidth', 2); % 插入右车道
% imshow(LanesRGB);
% title('刘斯旋-19221108-原始RGB图像车道检测结果');


%4.原始RGB图像显示车道检测结果
%将车道检测结果从鸟瞰图转换回原始图像坐标
% laneBoundaries = vehicleToImage(bevSensor.birdsEyeConfig, boundaries);
% %在初始图像上叠加车道线
% for i = 1:numel(laneBoundaries)
%     LanesOriginal = insertLaneBoundary(A, laneBoundaries(i), bevSensor.birdsEyeConfig, Xpoints, 'Color', 'r');
% end
% 
% imshow(LanesOriginal);
% title('刘斯旋-19221108-原始RGB图像车道检测图');
% 在原始RGB图像上绘制车道线



% sensor = bevSensor.birdsEyeConfig.Sensor;
% LanesOriginal = insertLaneBoundary(A,boundaries(1),sensor,Xpoints);
% LanesOriginal = insertLaneBoundary(LanesOriginal,boundaries(2),sensor,Xpoints,'Color','r');
% figure;
% imshow(LanesOriginal);
% title('刘斯旋-19221108-原始RGB图像车道检测图');

