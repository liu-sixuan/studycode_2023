function varargout = Tools(varargin)
% TOOLS MATLAB code for Tools.fig
%      TOOLS, by itself, creates a new TOOLS or raises the existing
%      singleton*.
%
%      H = TOOLS returns the handle to a new TOOLS or the handle to
%      the existing singleton*.
%
%      TOOLS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in TOOLS.M with the given input arguments.
%
%      TOOLS('Property','Value',...) creates a new TOOLS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Tools_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Tools_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Tools

% Last Modified by GUIDE v2.5 25-Sep-2023 11:20:25

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Tools_OpeningFcn, ...
                   'gui_OutputFcn',  @Tools_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Tools is made visible.
function Tools_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Tools (see VARARGIN)

% Choose default command line output for Tools
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Tools wait for user response (see UIRESUME)
% uiwait(handles.figure1);

global img;

% --- Outputs from this function are returned to the command line.
function varargout = Tools_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --------------------------------------------------------------------
function System_Callback(hObject, eventdata, handles)
% hObject    handle to System (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function File_Callback(hObject, eventdata, handles)
% hObject    handle to File (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function TypeTrans_Callback(hObject, eventdata, handles)
% hObject    handle to TypeTrans (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Operation_Callback(hObject, eventdata, handles)
% hObject    handle to Operation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Fourier_Callback(hObject, eventdata, handles)
% hObject    handle to Fourier (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Algebra_Callback(hObject, eventdata, handles)
% hObject    handle to Algebra (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Geometry_Callback(hObject, eventdata, handles)
% hObject    handle to Geometry (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Gray_Callback(hObject, eventdata, handles)
% hObject    handle to Gray (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%将彩色图像转为灰度图像
global imgGray 
imgGray= rgb2gray(img);
%打开一个新的窗口显示灰度图像
figure;
%显示转化后的灰度图像
imshow(imgGray);

% --------------------------------------------------------------------
function Binary_Callback(hObject, eventdata, handles)
% hObject    handle to Binary (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%使用getframe函数来获取当前的图像帧，
frame = getframe(handles.axes_Image);
%使用cdata属性获取显示的图像数据。
img_ = frame.cdata;
%imbinarize(I) 也可以在I后面手动设置阈值。默认用中值阈值法
BW = imbinarize(img_);
figure;
imshow(BW);

% --------------------------------------------------------------------
function Open_Callback(hObject, eventdata, handles)
% hObject    handle to Open (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%打开一个文件选择对话框，并限定用户只能选择扩展名为.bmp的图像文件
%对话框标题显示为'选择要打开的BMP文件'
[filename,pathname]=uigetfile('*.bmp','选择要打开的BMP文件');
%将路径和文件名组合在一起。datafile代表完整的路径
datafile=[pathname,filename];
%使用 datafile 中存储的完整文件路径来读取图像文件。
img=imread(datafile);
%显示图像
imshow(img);
% --------------------------------------------------------------------
function Save_Callback(hObject, eventdata, handles)
% hObject    handle to Save (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%保存当前图像
%使用getframe函数来获取当前的图像帧，
frame = getframe(handles.axes_Image);
%使用cdata属性获取显示的图像数据。
img_ = frame.cdata;
% 生成以时间戳作为的文件名
timestamp = datestr(now, 'yyyymmdd_HHMMSS');
% 生成完整的文件名。saved_image_ + 时间戳 +.bmp
filename = ['saved_image_', timestamp, '.bmp'];
% 保存图像
imwrite(img_, filename);
disp('图像保存成功！');

% --------------------------------------------------------------------
function SaveAs_Callback(hObject, eventdata, handles)
% hObject    handle to SaveAs (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%使用getframe函数来获取当前的图像帧，
frame = getframe(handles.axes_Image);
%使用cdata属性获取显示的图像数据。
img_ = frame.cdata;
%打开一个文件选择对话框，并限定用户只能保存.bmp文件。
%对话框标题显示为'另存为一个图像文件'
[filename, filepath] = uiputfile('*.bmp', '另存为一个图像文件');
%如果没有选择文件进行保存，则filename为0
if isequal(filename,0) || isequal(pathname,0)
    disp('图像保存被取消。');
    return;
else
    %构建保存文件的完整路径fpath
    fpath=fullfile(pathname, filename);
    %图像文件的写入.图像+路径+类型
    imwrite(img_,fpath,'bmp');
    disp('图像保存成功！');
end

% --------------------------------------------------------------------
function Exit_Callback(hObject, eventdata, handles)
% hObject    handle to Exit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

disp('按下任意键退出...');
% pause 暂停 等待用户按下任意键
pause;
% return 退出脚本
return;

% --------------------------------------------------------------------
function Zoom_Callback(hObject, eventdata, handles)
% hObject    handle to Zoom (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Rotation_Callback(hObject, eventdata, handles)
% hObject    handle to Rotation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Mirror_Callback(hObject, eventdata, handles)
% hObject    handle to Mirror (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Oppo_Callback(hObject, eventdata, handles)
% hObject    handle to Oppo (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Add_Callback(hObject, eventdata, handles)
% hObject    handle to Add (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Sub_Callback(hObject, eventdata, handles)
% hObject    handle to Sub (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
