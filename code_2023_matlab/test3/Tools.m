function varargout = Tools(varargin)
% TOOLS MATLAB code for Tools.fig
%      TOOLS, by itself, creates a new TOOLS or raises the existing
%      singleton*.
%
%      H = TOOLS returns the handle to a new TOOLS or the handle to
%      the existing singleton*.
%
%      TOOLS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in TOOLS.M with the given input arguments.
%
%      TOOLS('Property','Value',...) creates a new TOOLS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Tools_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Tools_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Tools

% Last Modified by GUIDE v2.5 18-Dec-2023 11:23:57

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Tools_OpeningFcn, ...
                   'gui_OutputFcn',  @Tools_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Tools is made visible.
function Tools_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Tools (see VARARGIN)

% Choose default command line output for Tools
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Tools wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Tools_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;
global img; %定义全局变量

% --------------------------------------------------------------------
function System_Callback(hObject, eventdata, handles)
% hObject    handle to System (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function File_Callback(hObject, eventdata, handles)
% hObject    handle to File (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Open_Callback(hObject, eventdata, handles)
% hObject    handle to Open (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%打开文件
global img; %定义全局变量
[filename, filepath] = uigetfile({'*.jpg;*.png;*.bmp', '选择要打开的图片文件'}, '选择要打开的图片文件'); % 打开文件对话框
if isequal(filename, 0)||isequal(path,0)  % 如果用户取消选择文件
    disp('未选择文件');
    return;
else
    fullPath = fullfile(filepath, filename); % 拼接完整的文件路径
    x = imread(fullPath);
    %axes(handles.axes1);%使用第一个axes
    imshow(x);    %显示图像
    img = x;
end
	 

% --------------------------------------------------------------------
function Save_Callback(hObject, eventdata, handles)
% hObject    handle to Save (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%保存文件
global img;
[file, path] = uiputfile({'*.bmp', 'BMP files';'*.jpg','JPG files'},'保存一个图像文件'); % 保存文件对话框
if isequal(file, 0)||isequal(path,0) % 如果用户取消保存文件
    disp('未保存文件');
    return;
else
    fullPath = fullfile(path, file); % 拼接完整的文件路径
    imwrite(img,fullPath,'bmp');
    disp(['保存文件：', fullPath]); 
end

% --------------------------------------------------------------------
function GrayTransformation_Callback(hObject, eventdata, handles)
% hObject    handle to GrayTransformation (see GCBO)
% eventdata  reserved - to btructure with handles and user data (see GUIDATA)
global img;
figure;
subplot(2,2,1);imshow(img);
title('原始图像');
subplot(2,2,2);imhist(img);%计算和显示图像的直方图
title('原始图像的灰度直方图');
imgnew = imadjust(img,[0.27 0.71],[0 1]);   %灰度变换函数，可以实现图像的直方图调节
subplot(2,2,3);imshow(imgnew);
subplot(2,2,4);imshow(imgnew);

%, -------------------------------------------------------------------
function Exit_Callback(hObject, eventdata, handles)
% hObject    handle to Exit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
close;

% --------------------------------------------------------------------
function Enhancement_Callback(hObject, eventdata, handles)
% hObject    handle to Enhancement (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structue defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Histogram_equal_Callback(hObject, eventdata, handles)
% hObject    handle to Histogram_equal (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% --------------------------------------------------------------------
function Gray_img_Callback(hObject, eventdata, handles)
% hObject    handle to Gray_img (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%直方图均衡：灰度图像
global img;

% 直方图均衡化
eq_img = histeq(img);

% 显示处理前后的图像以及灰度直方图
figure;
subplot(2,2,1);
imshow(img);
title('原始图像');

subplot(2,2,2);
imhist(img);
title('原始图像的灰度直方图');

subplot(2,2,3);
imshow(eq_img);
title('直方图均衡化后的图像');

subplot(2,2,4);
imhist(eq_img);
title('均衡化后的灰度直方图');

% R1 = img(:,:,1);    %红色分量
% G1 = img(:,:,2);    %绿色分量
% B1 = img(:,:,3);    %蓝色分量
% R1new = histeq(R1);
% G1new = histeq(G1);
% B1new = histeq(B1);
% 
% subplot(4,2,1);imhist(R1);  title('原始红色分量');
% subplot(4,2,2);imhist(R1new);   title('均衡化后的红色分量');
% subplot(4,2,3);imhist(G1);  title('原始绿色分量');
% subplot(4,2,4);imhist(G1new);   title('均衡化后的绿色分量');
% subplot(4,2,5);imhist(B1);  title('原始蓝色分量');
% subplot(4,2,6);imhist(B1new);   title('均衡化后的蓝色分量');
%imgnew = cat;

% --------------------------------------------------------------------
function Color_img_Callback(hObject, eventdata, handles)
% hObject    handle to Color_img (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%直方图均衡：彩色图像
global img;

% 判断图像是否为彩色图像
if size(img, 3) == 3
    disp('这是彩色图像');
    % 将图像转换到HSV颜色空间
    hsv_img = rgb2hsv(img);
    % 提取V通道
    val = hsv_img(:, :, 3);
    % 对V通道进行直方图均衡化
    eq_val = histeq(val);
    % 替换原图像的V通道
    hsv_img(:, :, 3) = eq_val;
    
    % 将图像转换回RGB颜色空间
    eq_img = hsv2rgb(hsv_img);
    
    % 显示原图像和均衡化后的图像
    figure;
    subplot(2,2,1);
    imshow(img);
    title('原始图像');

    subplot(2,2,2);
    imhist(img);
    title('原始图像的灰度直方图');

    subplot(2,2,3);
    imshow(eq_img);
    title('直方图均衡化后的图像');

    subplot(2,2,4);
    imhist(eq_img);
    title('均衡化后的灰度直方图');
else
    disp('这不是彩色图像，请重新打开彩色文件');
    h = msgbox('这不是彩色图像，请重新打开彩色文件', '错误', 'error');
    % 如果不是彩色图像，执行其他操作，例如退出窗口
    uiwait(h);
    close all;
end

% --------------------------------------------------------------------
function M_realization_Callback(hObject, eventdata, handles)
% hObject    handle to M_realization (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global img;

% 判断图像是否为彩色图像
if size(img, 3) == 3
    % 将图像转换到HSV颜色空间
    hsv_img = rgb2hsv(img);
    
    % 提取V通道
    val = hsv_img(:, :, 3);
    
    % 计算直方图
    num_bins = 256;
    counts = imhist(val, num_bins);
    
    % 计算累计直方图
    pdf = counts / numel(val);
    cdf = cumsum(pdf);
    
    % 将累计直方图缩放到[0, 1]范围内并进行四舍五入
    T = round((num_bins-1) * cdf);
    
    % 应用映射函数
    eq_val = T(val*255+1)/255;

    % 替换原图像的V通道
    hsv_img(:, :, 3) = eq_val;
    
    % 将图像转换回RGB颜色空间
    mappedImg = hsv2rgb(hsv_img);

    % 显示结果
% 显示原图像和均衡化后的图像
    figure;
    subplot(2,2,1);
    imshow(img);
    title('原始图像');
    
    subplot(2,2,2);
    imhist(img);
    title('原始图像的灰度直方图');
    
    subplot(2,2,3);
    imshow(mappedImg);
    title('直方图均衡化后的图像');
        
    subplot(2,2,4);
    imhist(mappedImg);
    title('均衡化后的灰度直方图');
% % 如果是彩色图像，则进行 BGR 到 LAB 转换
%     lab_img = rgb2lab(img);
%     l_channel = lab_img(:, :, 1); % 提取 L 通道
% 
%     % 转换 L 通道的像素值为 double，并进行线性缩放
%     l_channel = im2double(l_channel);
%     l_min = min(l_channel(:));
%     l_max = max(l_channel(:));
%     l_channel_scaled = (l_channel - l_min) / (l_max - l_min);
% 
%     % 计算灰度图像的直方图
%     counts = imhist(l_channel_scaled);
% 
%     % 计算归一化的累计直方图
%     pdf = counts / numel(l_channel_scaled);
%     cdf = cumsum(pdf);
% 
%     % 计算映射函数
%     T = uint8(255 * cdf);
% 
%     % 将 L 通道的像素值替换为映射函数中的值
%     l_channel_mapped = T(floor(l_channel_scaled * 255) + 1);
% 
%     % 更新 L 通道
%     lab_img(:, :, 1) = l_channel_mapped;
% 
%     % 转换回彩色图像
%     mappedImg = lab2rgb(lab_img);
else
    % 如果是灰度图像，则直接使用原始图像
    gray_img = img;
    % 计算灰度图像的直方图
    [counts, binLocations] = imhist(gray_img);
    
    % 计算归一化的累计直方图
    pdf = counts / sum(counts);
    cdf = cumsum(pdf);
    
    % 计算映射函数
    T = uint8(255 * cdf);
    
    % 将图像中每个像素的灰度值替换为映射函数中的值
    mappedImg = T(gray_img+1);
% 显示结果
% 显示原图像和均衡化后的图像
    figure;
    subplot(2,2,1);
    imshow(img);
    title('原始图像');
    
    subplot(2,2,2);
    imhist(img);
    title('原始图像的灰度直方图');
    
    subplot(2,2,3);
    imshow(mappedImg);
    title('直方图均衡化后的图像');
        
    subplot(2,2,4);
    imhist(mappedImg);
    title('均衡化后的灰度直方图');
end

% --------------------------------------------------------------------
function Trans_Callback(hObject, eventdata, handles)
% hObject    handle to Trans (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Gray_Callback(hObject, eventdata, handles)
% hObject    handle to Gray (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global img;
figure("Name",'彩色图像灰度化');
if ndims (img)==3   %是3D图像。即彩色
    y = rgb2gray(img);
    imshow(y);
else
    msgbox('这已经是灰度图像','无需转换');
end

% --------------------------------------------------------------------
function Binary_Callback(hObject, eventdata, handles)
% hObject    handle to Binary (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global img;
figure("Name",'二值化');

subplot(1,3,1);imshow(img);
title('原图像');

% 判断img类型，如果是彩色图像则转为灰度图像
if ndims(img) == 3 
    img = rgb2gray(img);
end

subplot(1,3,2);imshow(img);
title('灰度图像');

% 进行二值化
threshold = 0.5;
img = im2bw(img, threshold);
subplot(1,3,3);imshow(img);
title('二值图像');



% --------------------------------------------------------------------
function Smoothing_Callback(hObject, eventdata, handles)
% hObject    handle to Smoothing (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function NeighAver_Callback(hObject, eventdata, handles)
% hObject    handle to NeighAver (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

global img;
% 添加椒盐噪声
%[m,n,x] = size(imgnoise);
imgnoise = imnoise(img,'salt & pepper',0.02);   %添加椒盐噪声

%创建3*3/5*5/9*9的低通邻域模板
H1=ones(3,3)/9; %3*3 的邻域模板 或 H1=fspecial(‘average’,3);
H2=ones(5,5)/25; 
H3=ones(9,9)/81; 
% 进行平滑
imgnew1=imfilter(imgnoise,H1); %邻域平均
imgnew2=imfilter(imgnoise,H2); %邻域平均
imgnew3=imfilter(imgnoise,H3); %邻域平均

subplot(2,3,1);imshow(img);title('原始图像');
subplot(2,3,2);imshow(imgnoise);title('噪声图像');
subplot(2,3,4);imshow(uint8(imgnew1));title('3*3模板效果');
subplot(2,3,5);imshow(uint8(imgnew2));title('5*5模板效果');
subplot(2,3,6);imshow(uint8(imgnew3));title('9*9模板效果');

% --------------------------------------------------------------------
function MedianFilter_Callback(hObject, eventdata, handles)
% hObject    handle to MedianFilter (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global img;
imgnoise_salt = imnoise(img,'salt & pepper',0.02);   %添加椒盐噪声
imgnoise_gaussian = imnoise(img,'gaussian', 0, 0.02);   %添加高斯噪声
imgnoise_speckle = imnoise(img,'speckle', 0.02);   %添加乘性噪声

if size(img, 3) == 1
    %创建5*5中值滤波模板
    imgn1=medfilt2(imgnoise_salt,[5,5]);%用指定大小为 m*n 的窗口对图像 A 进行中值滤波
    imgn2=medfilt2(imgnoise_gaussian,[5,5]);
    imgn3=medfilt2(imgnoise_speckle,[5,5]);
else
    % 对彩色图像进行中值滤波
    % 将图像拆分成红色通道、绿色通道和蓝色通道分别进行中值滤波操作。
    % 然后使用 cat() 函数将处理后的通道重新合并成彩色图像。
    img_r = medfilt2(imgnoise_salt(:, :, 1), [5, 5]);
    img_g = medfilt2(imgnoise_salt(:, :, 2), [5, 5]);
    img_b = medfilt2(imgnoise_salt(:, :, 3), [5, 5]);
    imgn1 = cat(3, img_r, img_g, img_b);
      
    img_r = medfilt2(imgnoise_gaussian(:, :, 1), [5, 5]);
    img_g = medfilt2(imgnoise_gaussian(:, :, 2), [5, 5]);
    img_b = medfilt2(imgnoise_gaussian(:, :, 3), [5, 5]);
    imgn2 = cat(3, img_r, img_g, img_b);
    
    img_r = medfilt2(imgnoise_speckle(:, :, 1), [5, 5]);
    img_g = medfilt2(imgnoise_speckle(:, :, 2), [5, 5]);
    img_b = medfilt2(imgnoise_speckle(:, :, 3), [5, 5]);
    imgn3 = cat(3, img_r, img_g, img_b);
end
subplot(2,4,1);imshow(img);title('原始图像');
subplot(2,4,2);imshow(imgnoise_salt);title('椒盐噪声');
subplot(2,4,3);imshow(imgnoise_gaussian);title('高斯噪声');
subplot(2,4,4);imshow(imgnoise_speckle);title('乘性噪声');
subplot(2,4,5);imshow(uint8(imgn1));title('9*9模板椒盐噪声效果');
subplot(2,4,6);imshow(uint8(imgn2));title('9*9模板高斯噪声效果');
subplot(2,4,7);imshow(uint8(imgn3));title('9*9模板乘性噪声效果');

% --------------------------------------------------------------------
function NAverAndMedian_Callback(hObject, eventdata, handles)
% hObject    handle to NAverAndMedian (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global img;
imgnoise=imnoise(img,'salt & pepper',0.02);   %添加椒盐噪声
imgn_neighAver=imfilter(imgnoise,ones(5,5)/25); %邻域平均
if size(img, 3) == 1
    imgn_median=medfilt2(imgnoise,[5,5]);
else
    img_r = medfilt2(imgnoise(:, :, 1), [5, 5]);
    img_g = medfilt2(imgnoise(:, :, 2), [5, 5]);
    img_b = medfilt2(imgnoise(:, :, 3), [5, 5]);
    imgn_median = cat(3, img_r, img_g, img_b);
end
subplot(2,2,1);imshow(img);title('原始图像');
subplot(2,2,2);imshow(imgnoise);title('椒盐噪声');
subplot(2,2,3);imshow(uint8(imgn_neighAver));title('5*5模板邻域平均效果');
subplot(2,2,4);imshow(uint8(imgn_median));title('5*5模板中值滤波效果');

% --------------------------------------------------------------------
function ConcreteTest_Callback(hObject, eventdata, handles)
% hObject    handle to ConcreteTest (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
img_ = [1 2 2 2 3;
       1 15 1 2 2;
       2 1 2 0 3;
       0 2 2 3 1;
       3 2 0 2 2];
%均值滤波
H = 1/9 * ones(3, 3);
imgn_neigh1=filter2(H,img_,'same');
imgn_neigh2=filter2(H, img_, 'full');
imgn_median=medfilt2(img_,[3,3]);

subplot(1,4,1);imshow(img_);title('原始图像');
subplot(1,4,2);imshow(imgn_neigh1);title('均值滤波same处理');
subplot(1,4,3);imshow(imgn_neigh2);title('均值滤波full处理');
subplot(1,4,4);imshow(uint8(imgn_median));title('3*3模板中值滤波效果');


% --------------------------------------------------------------------
function Sharpen_Callback(hObject, eventdata, handles)
% hObject    handle to Sharpen (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Untitled_3_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Ideal_lowpass_filter_Callback(hObject, eventdata, handles)
% hObject    handle to Ideal_lowpass_filter (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global img;
% 添加噪声图像处理
noisy = imnoise(img, 'salt & pepper', 0.05); % 添加椒盐噪声

% 理想低通滤波器
F=fft2(noisy); % 傅里叶变换, noisy 是添加噪声的图像
F=fftshift(F); % 将图像频谱中心从矩阵原点移到矩阵中心
[M N]=size(F); % 求矩阵F 的大小
u1=fix(M/2); u2=fix(N/2);
D0_values = [10, 30, 50, 80, 200]; % 截止频率值
subplot(2,3,1);imshow(img);title('原始图像');
for i = 1:length(D0_values)
    D0 = D0_values(i);
    for u=1:M
        for v=1:N
            D=sqrt((u-u1)^2+(v-u2)^2);
            if (D<=D0) H=1;
            else H=0;
            end % 理想低通滤波器
            result(u,v)=H*F(u,v);
        end
    end
    result=ifftshift(result);
    J1=ifft2(result); % 傅里叶逆变换
    J2=uint8(real(J1));
    subplot(2,3,i+1);imshow(J2);title(['D0 = ',num2str(D0)]);
end



% --------------------------------------------------------------------
function Butterworth_Callback(hObject, eventdata, handles)
% hObject    handle to Butterworth (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global img;

% 添加噪声图像处理
img = imnoise(img, 'salt & pepper', 0.05); % 添加椒盐噪声

% Butterworth低通滤波器
D0_values = [10, 30, 50, 80, 200]; % 截止频率值
n = 2; % Butterworth滤波器的阶数
for i = 1:length(D0_values)
    D0 = D0_values(i);
    H = Butterworth_lowpass_filter(size(img), D0, n); % Butterworth低通滤波器函数
    filtered_img = Apply_filter(img, H); % 应用滤波器函数
    figure;
    subplot(1, 2, 1), imshow(img), title('原始图像');
    subplot(1, 2, 2), imshow(filtered_img), title(['Butterworth低通滤波器 (D0 = ' num2str(D0) ', n = ' num2str(n) ')']);
end

function H = Butterworth_lowpass_filter(size_img, D0, n)
% 计算Butterworth低通滤波器
P = size_img(1);
Q = size_img(2);
u = 0:(P-1);
v = 0:(Q-1);
idx = find(u > P/2);
u(idx) = u(idx) - P;
idy = find(v > Q/2);
v(idy) = v(idy) - Q;
[V, U] = meshgrid(v, u);
D = sqrt(U.^2 + V.^2);
H = 1 ./ (1 + (D./D0).^(2*n));

function filtered_img = Apply_filter(img, H)
% 应用滤波器
F_img = fftshift(fft2(img));
F_filtered = F_img .* H;
filtered_img = real(ifft2(ifftshift(F_filtered)));

% --------------------------------------------------------------------
function Lapras_Callback(hObject, eventdata, handles)
% hObject    handle to Lapras (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global img;
H = [0,1,0;1,-4,1;0,1,0];   %拉普拉斯算子
imgA = filter2(H,img);
imgnew = img-uint8(imgA);
subplot (1,3,1);imshow(img);title("原始图像");
subplot (1,3,2);imshow(uint8(imgA));title("拉普拉斯算子运算结果");
subplot (1,3,3);imshow(imgnew);title("增强后图像");


% --------------------------------------------------------------------
function Sobel_Callback(hObject, eventdata, handles)
% hObject    handle to Sobel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global img;
dx = [1 0 -1;2 0 -2;1 0 -1];
dy = [-1 -2 -1;0 0 0;1 2 1];
gradx = filter2(dx,img);
grady = filter2(dy,img);

subplot(1,4,1);imshow(img);
subplot(1,4,2);imshow(uint8(gradx));title('dx');
subplot(1,4,3);imshow(uint8(grady));title('dy');
subplot(1,4,4);imshow(uint8(gradx)+uint8(grady));title("sobel(dx+dy)");

% --------------------------------------------------------------------
function Prewitt_Callback(hObject, eventdata, handles)
% hObject    handle to Prewitt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global img;
dx = [1 0 -1;1 0 -1;1 0 -1];
dy = [-1 -1 -1;0 0 0;1 1 1];
gradx = filter2(dx,img);
grady = filter2(dy,img);

subplot(1,4,1);imshow(img);
subplot(1,4,2);imshow(uint8(gradx));title('dx');
subplot(1,4,3);imshow(uint8(grady));title('dy');
subplot(1,4,4);imshow(uint8(gradx)+uint8(grady));title("prewitt(dx+dy)");

% --------------------------------------------------------------------
function Gradient_Callback(hObject, eventdata, handles)
% hObject    handle to Gradient (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%在梯度法中，常用的五种图像锐化方法包括拉普拉斯算子、Sobel算子、Prewitt算子、
% Roberts算子和Scharr算子。
global img;
figure,subplot(2,3,1),imshow(img);
img = double(img);
[ix,iy]=gradient(img);
gm = sqrt(ix.*ix+iy.*iy);   %梯度
out1 = gm;
subplot(2,3,2);imshow(uint8(out1));
out2 = img;
j = find(gm>=10);%返回gm>=10的元素的相对应的线性索引值
out2(j)=gm(j);
subplot(2,3,3);imshow(uint8(out2));
out3 = img;
j = find(gm>=10);
out3(j)=255;
subplot(2,3,4);imshow(uint8(out3));
out4 = img;
j = find(gm<=10);
out4(j)=255;
subplot(2,3,5);imshow(uint8(out4));
out5=img;
j = find(gm>=10);
out5(j)=255;
q = find(gm<10);
out5(q)=0;
subplot(2,3,6);imshow(uint8(out5));


% --------------------------------------------------------------------
function Restoration_Callback(hObject, eventdata, handles)
% hObject    handle to Restoration (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Segmentation_Callback(hObject, eventdata, handles)
% hObject    handle to Segmentation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Simple_histogram_threshold_Callback(hObject, eventdata, handles)
% hObject    handle to Simple_histogram_threshold (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global img;
figure("Name",'简单直方图阈值分割');
subplot(1,3,1);imshow(img);
title('原图像');
subplot(1,3,2);imhist(img);
title('原图像的灰度直方图');
imgnew = im2bw(img,130/255);    %阈值需要根据灰度直方图调节
subplot(1,3,3);imshow(imgnew);
title('分割后图像');

% --------------------------------------------------------------------
function Otsu_Callback(hObject, eventdata, handles)
% hObject    handle to Otsu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global img;
figure("Name",'Otsu分割');
subplot(1,3,1);imshow(img);
title('原图像');
% 将图像转换为灰度图像
if ndims (img)==3   %是3D图像。即彩色
    gray_img = rgb2gray(img);
    subplot(1,3,2);imshow(gray_img);
    title('原图像的灰度图');
else
    gray_img = img;
    subplot(1,3,2);imshow(gray_img);
    title('原图像的灰度图');
end
% 使用 Otsu 分割法计算阈值
threshold = graythresh(gray_img);
% 进行二值化分割
binary_img = imbinarize(gray_img, threshold);
subplot(1,3,3);imshow(binary_img);
title('分割后图像');

% --------------------------------------------------------------------
function Edge_detection_operator_Callback(hObject, eventdata, handles)
% hObject    handle to Edge_detection_operator (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% 利用各种边缘检测 算子( ( Sobel 、 Prewitt 、 Roberts 、 log、Canny) ，对图像进行分割
global img;
figure("Name",'边缘检测算子分割');

% 转为灰度图像
% 将图像转换为灰度图像
if ndims (img)==3   %是3D图像。即彩色
    gray_img = rgb2gray(img);
    subplot(2,3,1);imshow(gray_img);
    title('原图像的灰度图');
else
    gray_img = img;
    subplot(2,3,1);imshow(gray_img);
    title('原图像的灰度图');
end

% 使用Sobel算子进行边缘检测
sobel_edge = edge(gray_img, 'Sobel');

% 使用Prewitt算子进行边缘检测
prewitt_edge = edge(gray_img, 'Prewitt');

% 使用Roberts算子进行边缘检测
roberts_edge = edge(gray_img, 'Roberts');

% 使用LoG算子进行边缘检测
log_edge = edge(gray_img, 'log');

% 使用Canny算子进行边缘检测
canny_edge = edge(gray_img, 'Canny');

% 显示结果
subplot(2, 3, 2), imshow(sobel_edge), title('Sobel');
subplot(2, 3, 3), imshow(prewitt_edge), title('Prewitt');
subplot(2, 3, 4), imshow(roberts_edge), title('Roberts');
subplot(2, 3, 5), imshow(log_edge), title('LoG');
subplot(2, 3, 6), imshow(canny_edge), title('Canny');

% --------------------------------------------------------------------
function Wiener_Callback(hObject, eventdata, handles)
% hObject    handle to Wiener (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global img;
figure("Name",'维纳滤波');
subplot(1,3,1),imshow(img);
title('原图像');
LEN = 50;   %设置运动位移为50个像素
THETA = 75; %设置运动角度为75度
img = double(img);
PSF = fspecial('motion',LEN,THETA); %建立二维仿真线性运动滤波器PSF
MF = imfilter(img,PSF,'circular','conv');    %用PSF产生退化图像
subplot(1,3,2),imshow(uint8(MF));
title("运动模糊图像");

Wnrl = deconvwnr(MF,PSF);   %用Wiener滤波消除运动模糊的图像
subplot(1,3,3),imshow(uint8(Wnrl));
title('复原后的图像');

% --------------------------------------------------------------------
function Sobel__Callback(hObject, eventdata, handles)
% hObject    handle to Sobel_ (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



% --------------------------------------------------------------------
function Prewitt__Callback(hObject, eventdata, handles)
% hObject    handle to Prewitt_ (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Roberts__Callback(hObject, eventdata, handles)
% hObject    handle to Roberts_ (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function log__Callback(hObject, eventdata, handles)
% hObject    handle to log_ (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Canny__Callback(hObject, eventdata, handles)
% hObject    handle to Canny_ (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Operation_Callback(hObject, eventdata, handles)
% hObject    handle to Operation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Untitled_5_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Untitled_11_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Add_Callback(hObject, eventdata, handles)
% hObject    handle to Add (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Sub_Callback(hObject, eventdata, handles)
% hObject    handle to Sub (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Zoom_Callback(hObject, eventdata, handles)
% hObject    handle to Zoom (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Rotation_Callback(hObject, eventdata, handles)
% hObject    handle to Rotation (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Mirror_Callback(hObject, eventdata, handles)
% hObject    handle to Mirror (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Opposition_Callback(hObject, eventdata, handles)
% hObject    handle to Opposition (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
