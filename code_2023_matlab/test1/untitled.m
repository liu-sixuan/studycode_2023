%1. 图像的读取、写入、显示
%(1). A＝imread(filename, fmt) 读入图像文件；
%(2). imwrite(A,filename, fmt) 输出图像；
%(3). imshow(A) 图像显示；
A_path=('D:\matlab\test1\lenna.bmp');
A = imread(A_path);
imwrite(A,A_path);
imshow(A);
%2. 图像类型转换
%(1). rgb2gray 函数
%B=rgb2gray(A) 将真彩色图像 A 转换成灰度图像 B
%(2). im2bw 函数
%通过设置亮度阈值将真彩色、索引色、灰度图转换成二值图像。

% 将彩色图像转换成灰度图像
gray = rgb2gray(A);
imshow(gray);
% 将灰度图像保存为 BMP 文件
imwrite(gray, 'gray_image.bmp');
%3. 文件对话框
%(1). uigetfile 函数 文件打开对话框
%(2). uiputfile 函数 文件保存对话框
[filename, filepath] = uigetfile('*.bmp', '选择要打开的 BMP 文件');
[filename, filepath] = uiputfile('*.bmp', '选择要保存的 BMP 文件');