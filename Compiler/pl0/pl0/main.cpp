#define _CRT_SECURE_NO_WARNINGS

#include "功能部分.h"
#include <iostream>

//extern int flag;
//extern string s1;
ofstream outfile;  //ofstream 用来写文件进outfile
int main()
{
	//选择测试文件。
	string filename;
	cout << "请选择测试文件（1~6）:" << endl;
	cin >> filename;
	//转为C风格字符串。
	//如果不使用.c_str()的话，在C++11以下的版本的编译器可能会出错。
	//为了保证代码的可移植性，使用c_str()
	ifstream in_file(filename.c_str());
	fstream io_file(filename.c_str(), ios::in | ios::out);

	if (io_file.fail())  //如果打开文件失败了，报错且退出程序。
	{
		cout << "文件打开失败!" << endl;
		exit(-1);
	}
	else  //文件打开成功，将文件展示出来，并关联输出文件。
	{
		cout << "打开文件如下：" << endl;
		//定义输出文件
		outfile.open("result.txt");
		cout << endl;
		outfile << endl;
	}

	string line;
	while (getline(io_file, line))
	{
		cout << line << endl;
	}

	while (getline(in_file, line))//读取文件中的一行
	{
		lexical_analysis(line);//词法分析
	}
	//-------------------------------------------------
	stack[n] = '\0';
	cout << "词法分析后的输入串为：";//一共n个输入字符
	outfile << "词法分析后的输入串为：";
	for (int i = 0; i < n; i++)
		cout << stack[i] << " ";
	cout << endl;
	//-------------------------------------------------
	Yufafenxi();//语法分析
	return 0;
}