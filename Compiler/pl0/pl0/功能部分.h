#pragma once
#include <iostream>
#include <string>
#include <math.h>
#include <fstream>
#include <iomanip>
#include <stdlib.h>

using namespace std;

extern int stack[100];
extern int n;
extern ofstream outfile;

void lexical_analysis(string s);	//把输入串分割
void word_judgement(string s);	//把分割好的字符进行分类
int changeGoto(char c);	//把非终结符转换到goto表中
int YuFa(int state, char& c, string& grammar); //求某个表达式规约时的个数
void Yufafenxi();