#define _CRT_SECURE_NO_WARNINGS 1
#include "all.h"

//关键字为：true false begin..end  if..then  or and not
#define key_num 9	//这里有9个关键字，可根据实际需要扩充。
#define key_len 10  //关键字的长度限定为10,也可根据实际需要更改。

int words_count = 0;//用来记录一共入栈了几个单词
int flag = 0;//错误标记，判断此法检错环节语法部分是否出错。记录语法是否错误。
int _flag = 0;	//记录词法是否错误。
string error_str;//记录出错的字符串

int line1 = 0;  //记录词法分析出错的行数。
// 关键字数组
char key_words[key_num][key_len] =
{
	"and","begin","end","false","if","not","or","then","true"
};

//词法分析-识别单词流
void lexical(string str)	//每次输入一行
{
	line1++;	//每分析一行，行数加一。
	char a_word[20];	//a_word 临时存储单词流
	int m = 0;	//a_word数组的下标

	/*一遍循环检测一个单词，单词都临时存入a_word中*/
	for (int i = 0; str[i] != '\0';)
	{
		while (str[i] == ' ' || str[i] == 10 || str[i] == 9) //忽略空格，换行(ASCII为10）和tab键（ASCII为9）
		{
			i++;
		}
		m = 0; a_word[m] = '\0';	//	更新临时存放单词的数组
	//***************************************************************************************
		//以字母开头的标识符
		if ((str[i] >= 'a' && str[i] <= 'z') || (str[i] >= 'A' && str[i] <= 'Z'))//标识符
		{
			a_word[m++] = str[i++];//复制单词
			//字母开头，后接字母或数字都是合法的标识符
			while ((str[i] >= 'a' && str[i] <= 'z') || (str[i] >= 'A' && str[i] <= 'Z') || (str[i] >= '0' && str[i] <= '9'))
			{
				a_word[m++] = str[i++];
			}	//遇见非字母、数字的符号时，说明一个单词读完了。
			a_word[m] = '\0';	//a_word加个\0，防止出现奇奇怪怪的字符。
			word_judgement(a_word);//检测出来一个完整的单词，送给word_judgement 判断单词的类别。
			while (str[i] == ' ' || str[i] == 10 || str[i] == 9) //忽略空格，换行(ASCII为10）和tab键（ASCII为9）
			{
				i++;
			}
		}

		//如果不是标识符，以数字开头，就只能是数字。
		else if (str[i] >= '0' && str[i] <= '9')
		{
			while (str[i] >= '0' && str[i] <= '9')	//	当前字符如果不是数字，则跳出循环
			{
				a_word[m++] = str[i++];	//	这个字符是数字
			}
			//当前字符不是数字了。可能是运算符、字符
			//如果后面直接跟了字母，说明是以数字开头的标识符，所以是错的。
			while (str[i] >= 'a' && str[i] <= 'z' || (str[i] >= 'A' && str[i] <= 'Z'))
			{
				a_word[m++] = str[i++];	//	
				flag = -1;
			}
			if (flag == -1) //前面那个单词是错的。
			{
				cout << "line " << line1 << "词法错误:标识符以字母开头，不能以数字开头。" << endl;
				outfile << "line " << line1 << "词法错误:标识符以字母开头，不能以数字开头。" << endl;
				flag = 0;	//恢复flag下次使用
				_flag = -1;		//记录是否有过词法错误
			}
			else    // 数字没错
			{
				a_word[m] = '\0';
				word_judgement(a_word);	
				while (str[i] == ' ' || str[i] == 10 || str[i] == 9) //忽略空格，换行(ASCII为10）和tab键（ASCII为9）
				{
					i++;
				}
			}
		}
//***************************************************************************************
		//运算符
		// <   <=   <> (即不等号)
		else if (str[i] == '<')
		{
			a_word[m++] = str[i++];
			if (str[i] == '=' || str[i] == '>')	//<=  <>
			{
				a_word[m++] = str[i++];
				a_word[m] = '\0';
				word_judgement(a_word);
				while (str[i] == ' ' || str[i] == 10 || str[i] == 9) //忽略空格，换行(ASCII为10）和tab键（ASCII为9）
				{
					i++;
				}
			}
			else
			{
				a_word[m] = '\0';
				word_judgement(a_word);
				while (str[i] == ' ' || str[i] == 10 || str[i] == 9) //忽略空格，换行(ASCII为10）和tab键（ASCII为9）
				{
					i++;
				}
			}
		}

		// >   >=
		else if (str[i] == '>')
		{
			a_word[m++] = str[i++];
			if (str[i] == '=')	//>=
			{
				a_word[m++] = str[i++];
				a_word[m] = '\0';
				word_judgement(a_word);
				while (str[i] == ' ' || str[i] == 10 || str[i] == 9) //忽略空格，换行(ASCII为10）和tab键（ASCII为9）
				{
					i++;
				}
			}
			else
			{
				a_word[m] = '\0';
				word_judgement(a_word);
				while (str[i] == ' ' || str[i] == 10 || str[i] == 9) //忽略空格，换行(ASCII为10）和tab键（ASCII为9）
				{
					i++;
				}
			}
		}

		// =  
		else if (str[i] == '=')
		{
			a_word[m++] = str[i++];
			a_word[m] = '\0';
			word_judgement(a_word);
			while (str[i] == ' ' || str[i] == 10 || str[i] == 9) //忽略空格，换行(ASCII为10）和tab键（ASCII为9）
			{
				i++;
			}
		}

		// # （ ）；   ——界符
		else if (str[i] == '#' || str[i] == ',' || str[i] == '(' || str[i] == ')' || str[i] == ';')
		{
			a_word[m++] = str[i++];
			a_word[m] = '\0';
			word_judgement(a_word);
			while (str[i] == ' ' || str[i] == 10 || str[i] == 9) //忽略空格，换行(ASCII为10）和tab键（ASCII为9）
			{
				i++;
			}
		}
		
		// +-*
		else if (str[i] == '+' || str[i] == '-' || str[i] == '*')
		{
			a_word[m++] = str[i++];
			a_word[m] = '\0';
			word_judgement(a_word);
			while (str[i] == ' ' || str[i] == 10 || str[i] == 9) //忽略空格，换行(ASCII为10）和tab键（ASCII为9）
			{
				i++;
			}
		}

		// := 赋值
		else if (str[i] == ':' )
		{
			a_word[m++] = str[i++];
			if (str[i] == '=')	// :=
			{
				a_word[m++] = str[i++];
				a_word[m] = '\0';
				word_judgement(a_word);
				while (str[i] == ' ' || str[i] == 10 || str[i] == 9) //忽略空格，换行(ASCII为10）和tab键（ASCII为9）
				{
					i++;
				}
			}
			else   //只有一个冒号，是不合法的语法
			{
				a_word[m] = '\0';
				cout << "line " << line1 << "词法错误: ':'不是合法的标识符" << endl;
				outfile << "line " << line1 << "词法错误: ':'不是合法的标识符" << endl;
				_flag = -1;
			}
		}
		
		//// 无法识别的异常符号 $/%/@/^/!等
		else// if (str[i] == '$' || str[i] == '%' || str[i] == '@')
		{
			cout <<"line "<< line1 << "词法错误：出现无法识别的异常符号" <<str [i] << endl;
			outfile << "line " << line1 <<"词法错误：出现无法识别的异常符号" << str[i] << endl;
			//跳过这个字符继续读下一个
			i++;
			_flag = -1;
		}

	}

}

//检测关键字
void word_judgement(string s)
{
	if (s == "begin")
	{
		cout << "关键字:  " << s << endl;
		outfile << "关键字:  " << s << endl;
		stack[1][words_count] = line1;
		stack[0][words_count++] = 0;
	}
	else if (s == "end")
	{
		cout << "关键字：" << s << endl;
		outfile << "关键字：" << s << endl;
		stack[1][words_count] = line1;
		stack[0][words_count++] = 1;	//
	}
	else if (s == "if")
	{
		cout << "关键字：" << s << endl;
		outfile << "关键字：" << s << endl;
		stack[1][words_count] = line1;
		stack[0][words_count++] = 2;
	}
	else if (s == "then")
	{
		cout << "关键字：" << s << endl;
		outfile << "关键字：" << s << endl;
		stack[1][words_count] = line1;
		stack[0][words_count++] = 3;
	}
	else if (s == ";")
	{
		cout << "界符：" << s << endl;
		outfile << "界符：" << s << endl;
		stack[1][words_count] = line1;
		stack[0][words_count++] = 5;
	}
	else if (s == ":=")	
	{
		cout << "运算符：" << s << endl;
		outfile << "运算符：" << s << endl;
		stack[1][words_count] = line1;
		stack[0][words_count++] = 6;
	}
	else if (s == "+")
	{
		cout << "运算符：" << s << endl;
		outfile << "运算符：" << s << endl;
		stack[1][words_count] = line1;
		stack[0][words_count++] = 7;
	}
	else if (s == "*")
	{
		cout << "运算符：" << s << endl;
		outfile << "运算符：" << s << endl;
		stack[1][words_count] = line1;
		stack[0][words_count++] = 8;
	}
	else if (s == "-")
	{
		cout << "运算符：" << s << endl;
		outfile << "运算符：" << s << endl;
		stack[1][words_count] = line1;
		stack[0][words_count++] = 9;
	}
	else if (s == "(")
	{
		cout << "界符：" << s << endl;
		outfile << "界符：" << s << endl;
		stack[1][words_count] = line1;
		stack[0][words_count++] = 10;
	}
	else if (s == ")")
	{
		cout << "界符：" << s << endl;
		outfile << "界符：" << s << endl;
		stack[1][words_count] = line1;
		stack[0][words_count++] = 11;
	}
	else if (s == "or")
	{
		cout << "关键字：" << s << endl;
		outfile << "关键字：" << s << endl;
		stack[1][words_count] = line1;
		stack[0][words_count++] = 12;
	}
	else if (s == "and")
	{
		cout << "关键字：" << s << endl;
		outfile << "关键字：" << s << endl;
		stack[1][words_count] = line1;
		stack[0][words_count++] = 13;
	}
	else if (s == "not")
	{
		cout << "关键字：" << s << endl;
		outfile << "关键字：" << s << endl;
		stack[1][words_count] = line1;
		stack[0][words_count++] = 14;
	}
	else if (s == ">=" || s == ">" || s == "=" || s == "<>" || s == "<=" || s == "<")
	{
		cout << "运算符：" << s << endl;
		outfile << "运算符：" << s << endl;
		stack[1][words_count] = line1;
		stack[0][words_count++] = 15;
	}
	else if (s == "#")
	{
		cout << "界符: # " << endl;
		outfile << "界符:# " << endl;
		stack[1][words_count] = line1;
		stack[0][words_count++] = 18;

	}
	else if (s == "true")
	{
		cout << "关键字：true" << endl;
		outfile << "关键字：true" << endl;
		stack[1][words_count] = line1;
		stack[0][words_count++] = 16;
	}
	else if (s == "false")
	{
		cout << "关键字：false" << endl;
		outfile << "关键字：false" << endl;
		stack[1][words_count] = line1;
		stack[0][words_count++] = 17;
	}
	else if ((s >= "a" && s <= "z") || (s >= "0" && s <= "9") || (s >= "A" && s <= "Z"))
	{
		if ((s >= "a" && s <= "z") || (s >= "A" && s <= "Z"))
		{
			cout << "标识符：" << s << endl;
			outfile << "标识符：" << s << endl;
		}
		else if ((s >= "0" && s <= "9"))
		{
			cout << "数字：" << s << endl;
			outfile << "数字：" << s << endl;
		}
		stack[1][words_count] = line1;
		stack[0][words_count++] = 4;
	}
	else
	{
		if (s != "\t" || s != "" || s != " ")
		{
			flag = 1;
			error_str = s;
		}//如果不是上面任意一种情况,语法出错，为后面做铺垫
	}
}
