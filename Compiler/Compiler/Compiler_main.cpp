﻿// Compiler_main.cpp : 此文件包含 "main" 函数。程序执行将在此处开始并结束。
//

#include "all.h"
ofstream outfile;  //用来写文件进outfile,与最终的结果文件 "result.txt"相关联。
string fileName;
extern int _flag;
int main()
{
    /*选择测试文件。*/
    int choose;
    while (true)
    {
        cout << "请选择测试文件（1~5）:" << endl;
        cin >> choose;
        if (choose >= 1 && choose <= 5)
        {
            fileName = to_string(choose) + ".txt";
            cout << "选择的文件名为：" << fileName << endl;
            break;
        }
        else {
            cout << "输入的数字无效,请重新输入！" << endl;
        }
    }
    //转为C风格字符串。
    //如果不使用.c_str()的话，在C++11以下的版本的编译器可能会出错。
    //为了保证代码的可移植性，使用c_str()
    ifstream in_file(fileName.c_str());  //  只读
    fstream io_file(fileName.c_str(), ios::in | ios::out); //可读可写

    /*判断文件是否成功打开*/
    if (io_file.fail())  //如果打开文件失败了
    {
        cout << "文件打开失败!退出程序" << endl;
        exit(-1);
    }
    else    //文件打开成功，将文件展示出来，并关联输出文件。
    {
        /*显示选择的文件*/
        cout << "打开文件如下:" << endl;
        /*关联输出文件*/
        outfile.open("result.txt"); //以写模式打开result.txt,并将其与outfile关联起来。
    }

    /*展示打开的文件*/
    string line;
    while (getline(io_file, line))   //循环读取文件中的每一行，并输出
    {
        cout << line << endl;
    }

    /*weil用数字表示保留字等*/
    cout << endl << "-----------------------------------------------------------------------------" << endl
        << "单词与数字对应关系如下：" << endl
        << "begin ->0   end ->1    if ->2     then ->3    id ->4" << endl
        << "; ->5       := ->6      + ->7      * ->8       - ->9" << endl
        << "( ->10      ) ->11     or ->12    and ->13    not ->14" << endl
        << "rop ->15    true ->16  false ->17 # ->18" << endl
        << "-----------------------------------------------------------------------------" << endl
        << "非终结符与数字对应关系如下：" << endl
        << "S-> -1   C-> -2   L-> -3   A-> -4   B-> -5   K-> -6   E-> -7" << endl

        /*打印表达式*/
        << "-----------------------------------------------------------------------------" << endl
        << "表达式如下：" << endl
        << "1.S->CS    2.S->begin L end  3.S->A         4.C->if B then" << endl
        << "5.L->S     6.L->K S          7.K->L;        8.A->id:=E" << endl
        << "9.E->E+E   10.E->E*E         11.E->-E       12.E->(E) " << endl
        << "13E->id    14.B->B or B      15.B->B and B  16.B->not B" << endl
        << "17.B->(B)  18.B->E rop E     19.B->true     20.B->false" << endl
        << "-----------------------------------------------------------------------------" << endl << endl;
    /*写入outfile 的文件*/
    outfile << "-----------------------------------------------------------------------------" << endl
        << "单词与数字对应关系如下：" << endl
        << "begin ->0   end ->1    if ->2     then ->3    id ->4" << endl
        << "; ->5       := ->6      + ->7      * ->8       - ->9" << endl
        << "( ->10      ) ->11     or ->12    and ->13    not ->14" << endl
        << "rop ->15    true ->16  false ->17 # ->18" << endl
        << "-----------------------------------------------------------------------------" << endl
        << "非终结符与数字对应关系如下：" << endl
        << "S-> -1   C-> -2   L-> -3   A-> -4   B-> -5   K-> -6   E-> -7" << endl
        << "-----------------------------------------------------------------------------" << endl
        << "表达式如下：" << endl
        << "1.S->CS    2.S->begin L end  3.S->A         4.C->if B then" << endl
        << "5.L->S     6.L->K S          7.K->L;        8.A->id:=E" << endl
        << "9.E->E+E   10.E->E*E         11.E->-E       12.E->(E) " << endl
        << "13E->id    14.B->B or B      15.B->B and B  16.B->not B" << endl
        << "17.B->(B)  18.B->E rop E     19.B->true     20.B->false" << endl
        << "-----------------------------------------------------------------------------" << endl << endl;

    //词法分析
    cout << "词法分析：" << endl;
    outfile << "词法分析：" << endl;
    /*每一行进行词法分析*/
    while (getline(in_file, line))//读取文件中的一行
    {
        lexical(line);//词法分析
    }
    stack[1][words_count] = line1;  //  在最后一个已有字符的后一个位置再扩充一个行，防止由于最后一个字符后面缺少字符而导致的错误
    /*如果词法有误直接退出程序，不进行语法分析。*/
    if (_flag == -1)
    {
        cout << endl << fileName<< "词法有误！请修改错误后再进行分析。"<<endl;
        exit(-1);
    }
    else
    {
        cout << endl << fileName << "词法检查正确！" << endl;
        outfile << endl << fileName << "词法检查正确！" << endl;
    }
    cout << "输入缓冲区的单词串：";
    outfile << "输入缓冲区的单词串：";
    for (int i = 0; i < words_count; i++)
        cout << stack[0][i] << " ";
    cout << endl << endl << endl;
    outfile << endl << endl << endl;

    //语法分析
    cout << "语法分析：" << endl;
    outfile << "语法分析：" << endl;
    grammatical();//语法分析
    system("pause");
    return 0;
}

