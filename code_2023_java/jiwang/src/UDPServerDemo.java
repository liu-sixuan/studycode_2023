import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class UDPServerDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			DatagramSocket ssocket = new DatagramSocket(7080);
			DatagramPacket packet = new DatagramPacket(new byte[1024], 1024);
			System.out.println("UDPServer is running on");
			ssocket.receive(packet);//通过服务器端口号（7080）接收数据报 
			int port=packet.getPort(); //获取客户端的端口号
			System.out.println(
					packet.getAddress().getHostName() + "(" + port + "):" + new String(packet.getData()));
			packet.setData("Hello Client! It send from UDP Server".getBytes()); //发往客户端的数据报
			packet.setPort(port); //设置客户端的端口号
			packet.setAddress(InetAddress.getLocalHost());
			ssocket.send(packet); //通过服务器端口号（7080）发送
			ssocket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
