//package ch10;

/*
  DatagramSocket(UDP)简单示例
     客户端:输入字符串到服务器，返回大写
*/
import java.io.*;
import java.net.*;
import java.util.Scanner;

public class UDPInputClient {
	public static void main(String[] args) {
		String hostname = "127.0.0.1";  //"localhost";
		int port = 7080; // 服务器端口
		try {
			InetAddress ia = InetAddress.getByName(hostname); // .getLocalHost();
			DatagramSocket socket = new DatagramSocket();// 随机端口号
			Scanner input = new Scanner(System.in);
			while (true) {
				System.out.print("请输入字符串（bye或end为结束）：");
				String message = input.nextLine();
				byte[] bufsend = message.getBytes();
				// 用于发送数据的数据报
				DatagramPacket packet = new DatagramPacket(bufsend, message.length(), ia, port);
				socket.send(packet); // 发送给服务器的数据报

				if (message.equals("bye") || message.equals("end"))
					break;
				// 用于接收数据的数据报
				byte[] bufrec = new byte[1024];
				DatagramPacket receivePacket = new DatagramPacket(bufrec, bufrec.length);
				socket.receive(receivePacket);
				// String received = new String(bufrec, 0,receivePacket.getLength());
				// System.out.println("从服务器返回的字符串：" + received);

				System.out.println("从服务器(IP:" + receivePacket.getAddress().getHostName() + ":" + receivePacket.getPort()
						+ ")返回的字符串：" + new String(receivePacket.getData()));
			}
			socket.close();
			input.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}