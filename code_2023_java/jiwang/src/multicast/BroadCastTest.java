package multicast;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import java.awt.Font;

public class BroadCastTest {
	private JTextField portTxtFiled = new JTextField();
	private JTextField CastIPTxtFiled = new JTextField();
	private JTextField nameField = new JTextField();
	private JButton startChatBtn = new JButton("开始聊天");
	private JButton stopChatBtn = new JButton("断开");
	private JTextArea receiveMesArea = new JTextArea();

	JScrollPane jScrollPane = new JScrollPane(receiveMesArea);
	private JTextArea sendMesArea = new JTextArea();
	private JButton clearScrBtn = new JButton("清屏");
	private JButton sendBtn = new JButton("发送");
	private JButton quitBtn = new JButton("退出");

	private BroadCast broadCast = null;  //调用BroadCast.java线程

	@SuppressWarnings("deprecation")
	public void InitFrame() {
		receiveMesArea.setFont(new Font("Times New Roman", Font.PLAIN, 12));
		JFrame mainFrame = new JFrame();
		mainFrame.setTitle("\u7EC4\u64AD\u804A\u5929\u793A\u4F8B");

		mainFrame.getContentPane().setLayout(null);
		mainFrame.setSize(492, 447);
		mainFrame.getContentPane().setLayout(null);

		JLabel JLabel32 = new JLabel("组播昵称");
		JLabel32.setBounds(20, 10, 60, 20);
		mainFrame.getContentPane().add(JLabel32);
		nameField.setBounds(80, 10, 130, 20);
		mainFrame.getContentPane().add(nameField);

		JLabel JLabel2 = new JLabel("端口号");
		JLabel2.setBounds(271, 10, 40, 20);
		mainFrame.getContentPane().add(JLabel2);
		portTxtFiled.setBounds(321, 10, 71, 20);
		mainFrame.getContentPane().add(portTxtFiled);

		JLabel JLabel3 = new JLabel("组播地址");
		JLabel3.setBounds(20, 47, 60, 20);
		mainFrame.getContentPane().add(JLabel3);
		CastIPTxtFiled.setFont(new Font("Arial", Font.PLAIN, 16));
		CastIPTxtFiled.setText("226.0.0.1");
		CastIPTxtFiled.setBounds(80, 40, 108, 33);
		mainFrame.getContentPane().add(CastIPTxtFiled);

		startChatBtn.setBounds(88, 94, 100, 20);
		stopChatBtn.setBounds(255, 94, 100, 20);
		mainFrame.getContentPane().add(startChatBtn);
		mainFrame.getContentPane().add(stopChatBtn);

		JLabel JLabel4 = new JLabel("接收发消息");
		JLabel4.setBounds(30, 124, 100, 20);
		jScrollPane.setBounds(40, 154, 418, 107);
		jScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		mainFrame.getContentPane().add(jScrollPane);
		mainFrame.getContentPane().add(JLabel4);

		JLabel JLabel5 = new JLabel("发送消息");
		JLabel5.setBounds(40, 271, 100, 20);
		sendMesArea.setFont(new Font("Arial", Font.PLAIN, 12));
		sendMesArea.setLineWrap(true);
		sendMesArea.setBounds(40, 301, 418, 61);
		mainFrame.getContentPane().add(sendMesArea);
		mainFrame.getContentPane().add(JLabel5);

		clearScrBtn.setBounds(65, 372, 100, 20);// 清屏
		mainFrame.getContentPane().add(clearScrBtn);
		sendBtn.setBounds(200, 372, 100, 20); // 发送
		mainFrame.getContentPane().add(sendBtn);
		quitBtn.setBounds(333, 372, 100, 20); // 退出
		mainFrame.getContentPane().add(quitBtn);
		mainFrame.show();
		startChatBtn.setEnabled(true);
		stopChatBtn.setEnabled(false);
		sendBtn.setEnabled(false);
		
		JLabel lblNewLabel = new JLabel("224.0.0.1\u6216224.0.2.0\uFF5E238.255.255.255");
		lblNewLabel.setBounds(199, 49, 234, 28);
		mainFrame.getContentPane().add(lblNewLabel);

		startChatBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				StartChat();
			}
		});
		sendBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				SendMessage();
			}
		});
		clearScrBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				ClearScreen();
			}
		});
		stopChatBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				startChatBtn.setEnabled(true);
				stopChatBtn.setEnabled(false);
				sendBtn.setEnabled(false);
				StopChat();
			}
		});
		quitBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				StopChat();
				System.exit(0);
			}
		});
	}

	public static void main(String[] args) {
		BroadCastTest client = new BroadCastTest();
		client.InitFrame();
	}

	public void StartChat() {
		String port = portTxtFiled.getText();
		String castIp = portTxtFiled.getText();
		if (!castIp.equals("") && !port.equals("")) {
			startChatBtn.setEnabled(false);
			stopChatBtn.setEnabled(true);
			sendBtn.setEnabled(true);
			broadCast = new BroadCast(Integer.parseInt(portTxtFiled.getText()), CastIPTxtFiled.getText(),
					nameField.getText(), receiveMesArea);
			broadCast.start();
		}
	}

	public void SendMessage() {
		broadCast.sendMessage(sendMesArea.getText());
	}

	public void ClearScreen() {
		if (broadCast.isAlive())
		broadCast.ClearScreen();
	}

	@SuppressWarnings("deprecation")
	public void StopChat() {
       //		broadCast.StopChat();
		broadCast.stop();
		broadCast=null;   //.stop();
	}
}