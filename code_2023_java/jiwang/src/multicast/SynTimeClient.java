package multicast;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import java.io.*;
import java.net.Socket;

public class SynTimeClient extends JFrame implements ActionListener{

	JFrame jf=null;
	JPanel jp=null;
	JButton jb=null;
	JLabel jl=null;
	JTextField jtf=null;
	
	public static void main(String args[])
	{
		SynTimeClient t=new SynTimeClient();
	}
	public SynTimeClient()
	{
		jf=new JFrame();
		jp=new JPanel();
		jb=new JButton("单击开始同步");
		jl=new JLabel("时间 :  ");
		jtf=new JTextField(13);
		
		this.setLayout(new FlowLayout(1,20,30));
		this.add(jl);
		this.add(jtf);
		this.add(jb);
		jb.addActionListener(this);
		this.setTitle("Client");
		this.setSize(270, 170);
		this.setVisible(true);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setLocationRelativeTo(null);
		this.setResizable(false);
	}
	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		PrintWriter pw=null;
		if(arg0.getSource()==jb)
		{
			try {
				Socket s=new Socket("127.0.0.1",9988);
				//pw=new PrintWriter(s.getOutputStream(),true);
				BufferedReader bf=new BufferedReader(new InputStreamReader(s.getInputStream()));
				String info=bf.readLine();
				jtf.setText(info);
				jtf.repaint();
				s.close();
			} catch (Exception e) {
				// TODO: handle exception
			}finally{
				
			}
		}
	}
}
