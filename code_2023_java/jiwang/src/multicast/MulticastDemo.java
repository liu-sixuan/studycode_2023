package multicast;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.UnknownHostException;
import java.util.concurrent.TimeUnit;

public class MulticastDemo {
	private static int PORT = 5555;
	private static String MULTICAST_ADDRESS = "225.0.0.1";

	static class MulticastClient {

		private MulticastSocket socket;
		private String id;
		private InetAddress group;
		private int port;

		public MulticastClient(final String id, InetAddress group, int port) throws IOException {
			this.id = id;
			this.group = group;
			this.port = port;
			socket = new MulticastSocket(port);
			socket.joinGroup(group);
			final byte[] buf = new byte[256];
			new Thread(new Runnable() {
				@Override
				public void run() {
					while (true) {
						try {
							DatagramPacket packet = new DatagramPacket(buf, buf.length);
							socket.receive(packet);
							String msg = new String(packet.getData());
							System.out.println("客户端" + id + "接受到的数据：" + msg);
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
			}).start();

		}

		public void send(String msg) {
			byte[] bytes = msg.getBytes();
			try {
				socket.leaveGroup(group);// 发送的人不接收组播消息
				msg = "客户端-" + id + " 发送数据：" + msg;
				System.out.println(msg);
				socket.send(new DatagramPacket(bytes, bytes.length, group, port));
				socket.joinGroup(group);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static void main(String[] args) throws UnknownHostException, IOException, InterruptedException {
		MulticastClient mcA = new MulticastClient("A", InetAddress.getByName(MULTICAST_ADDRESS), PORT);
		MulticastClient mcB = new MulticastClient("B", InetAddress.getByName(MULTICAST_ADDRESS), PORT);
		MulticastClient mcC = new MulticastClient("C", InetAddress.getByName(MULTICAST_ADDRESS), PORT);

		mcA.send("Hello A!");
		TimeUnit.SECONDS.sleep(1);
		mcA = null;
		mcB.send("Hello! B");
		TimeUnit.SECONDS.sleep(2);
		mcC.send("Hello! C");
		TimeUnit.SECONDS.sleep(2);
		mcB = null;
		mcC = null;
		System.exit(2);
	}

}
// 原文链接：https://blog.csdn.net/eussi/java/article/details/83244136