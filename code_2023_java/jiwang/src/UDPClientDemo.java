//package ch10;

/*
  DatagramSocket(UDP)简单示例
     客户端:7090,或者随机选择一个
*/
import java.io.*;
import java.net.*;

public class UDPClientDemo {
	public static void main(String[] args) {
		String hostname = "localhost"; // "127.0.0.1";
		int port = 7080; // 服务器端口
		try {
			InetAddress ia = InetAddress.getByName(hostname);
			DatagramSocket csocket = new DatagramSocket();//随机端口号
			// DatagramSocket client = new DatagramSocket(7090);
			DatagramPacket packet = new DatagramPacket(new byte[1024], 1024, ia, port);
			packet.setData("Hello Server! I send from UDP Client".getBytes() ); // 发送给服务器的数据
			csocket.send(packet); // 发送给服务器的数据报
			csocket.receive(packet); // 接收服务器的数据报
			System.out.println(
					packet.getAddress().getHostName() + "(" + packet.getPort() + "):" + new String(packet.getData()));
			csocket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}