//package ch10;

import java.net.*;

public class UDPInputServer {
	public static void main(String[] args) {
		byte[] buf = new byte[1024];
		int port = 7080;

		try {
			DatagramSocket socket = new DatagramSocket(port);

			System.out.println("服务器在7080 端口等待...");
			String datalowcase;
			while (true) {
				// 用于接收数据的数据报
				DatagramPacket packet = new DatagramPacket(buf, 1024);
				socket.receive(packet);// 通过服务器端口号（7080）接收数据报
				port = packet.getPort(); // 获取客户端的端口号
				String data = new String(buf, 0, packet.getLength());
				datalowcase = data.toLowerCase();
				if (datalowcase.equals("bye") || datalowcase.equals("end"))
					break;
				System.out.println("客户数据：" + data);
				String send = data.toUpperCase();
				InetAddress clientIP = packet.getAddress(); // 返回客户端的IP地址
				System.out
						.println(packet.getAddress().getHostName() + "(" + port + "):" + new String(packet.getData()));

				byte[] msg = send.getBytes();
				// 用于发送数据的数据报
				DatagramPacket sendPacket = new DatagramPacket(msg, msg.length, clientIP, port);
				socket.send(sendPacket);
			}
			socket.close();
			System.out.println("Server is closed.");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
