/*
 * 一个简易Web服务器，用浏览器作为客户端进行访问
 */

import java.net.*;
import java.io.*;

public class MyThreadWebServer {
	public static void main(String args[]) {
		ServerSocket webSocket;
		try {
			webSocket = new ServerSocket(8088);
			System.out.println("Web Server PORT 8088 OK");
			while (true) {
				Socket toClientSocket = webSocket.accept();
				Sprocess sp = new Sprocess(toClientSocket);
				Thread th = new Thread(sp);
				th.start();
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
}

class Sprocess implements Runnable {
	Socket csocket;

	public Sprocess(Socket sc) {
		csocket = sc;
	}

	public void run() {
		try {
			PrintStream out = new PrintStream(csocket.getOutputStream());
			BufferedReader in = new BufferedReader(new InputStreamReader(csocket.getInputStream()));
			String info = in.readLine();
			System.out.println("now got " + info);
			out.println("HTTP/1.0 200 OK"); // 这三行是HTTP协议头部，不能省略
			out.println("MIME_version:1.0");
			out.println("Content_Type:text/html");
			// 浏览器请求形如 GET /t/1.html HTTP/1.1
			// sp1, sp2为第一次和第二次出现空格的位置，
			// filename从浏览器请求中提取出文件路径和名称 如 t/1.html
			int sp1 = info.indexOf(' ');
			int sp2 = info.indexOf(' ', sp1 + 1);
			String filename = info.substring(sp1 + 2, sp2);
			// 若浏览器请求中无文件名，则加上默认文件名index.html
			if (filename.equals("") || filename.endsWith("/"))
				filename += "html\\index.html";
			System.out.println("Sending " + filename);
			// 向浏览器发送文件
			File fi = new File(filename);
			InputStream fs = new FileInputStream(fi);
			int n = fs.available();
			byte buf[] = new byte[1024];
			out.println("Content_Length:" + n);
			out.println("");
			while ((n = fs.read(buf)) >= 0) {
				out.write(buf, 0, n);
			}
			fs.close();
			out.close();
			in.close();
			csocket.close();
		} catch (IOException e) {
			System.out.println("Exception:" + e);
		}
	}
}
