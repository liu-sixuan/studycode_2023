package JR19221108;
/*19221108-计科211-刘斯旋*/

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.BevelBorder;
import javax.swing.border.EtchedBorder;

import java.net.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.io.*;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import javax.swing.SwingConstants;
import javax.swing.JSpinner;
import javax.swing.JSplitPane;

import javax.swing.JOptionPane;

public class JR19221108 extends JFrame {
	private JTextField txtUrl;
	private JLabel labTip;
	JTextArea txAin;
	JTextArea txAout;

	/**
	 * Launch the application
	 * 
	 * @param args
	 */
	public static void main(String args[]) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JR19221108 frame = new JR19221108();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame
	 */
	public JR19221108() {
		super();
		setTitle("EX13-WebReader");
		setBounds(100, 100, 658, 439);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(null);
		final JLabel labTip = new JLabel();
		labTip.setBounds(0, 0, 0, 0);
		labTip.setName("labTip");
		labTip.setText("input web page");
		getContentPane().add(labTip);
		
		JSplitPane splitPane = new JSplitPane();
		splitPane.setBounds(0, 37, 644, 332);
		splitPane.setResizeWeight(0.7);
		splitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
		getContentPane().add(splitPane);
		
		JScrollPane scrollPaneA = new JScrollPane();
		splitPane.setLeftComponent(scrollPaneA);
		
		txAin = new JTextArea();
		scrollPaneA.setViewportView(txAin);
		txAin.setLineWrap(true);
		
		JScrollPane scrollPaneB = new JScrollPane();
		splitPane.setRightComponent(scrollPaneB);
		
		txAout = new JTextArea();
		scrollPaneB.setViewportView(txAout);
		
		JPanel panButton = new JPanel();
		panButton.setBounds(0, 369, 644, 33);
		getContentPane().add(panButton);
		
		//打开文件功能
		JButton btnOpenFile = new JButton("OpenFile");
		panButton.add(btnOpenFile);

		//直接打开固定文件
		btnOpenFile.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
		        // 获取程序所在的工作目录
		        String projectPath = System.getProperty("user.dir");
		        // 指定需要打开的文件路径（注意：这里使用相对路径）
		        String filePath = projectPath + "t/19221108/商用密码管理条例ANSI"+ ".txt";
		        File file = new File(filePath);
		        try {
		            // 使用BufferedReader读取文件内容
		            BufferedReader reader = new BufferedReader(new FileReader(file));
		            StringBuilder sb = new StringBuilder();
		            String line;
		            while ((line = reader.readLine()) != null) {
		                sb.append(line).append("\n");
		            }
		            reader.close();
		            // 将读取的文件内容显示到文本区域中
		            txAin.setText(sb.toString());
		        } catch (IOException ex) {
		            ex.printStackTrace();
		            // 处理读取文件异常
		            JOptionPane.showMessageDialog(null, "文件读取失败！", "错误", JOptionPane.ERROR_MESSAGE);
		        }
		    }
		});
		/*
		JButton btnOpenFile = new JButton("OpenFile");
		btnOpenFile.setFont(new Font("Calibri", Font.PLAIN, 18));
		panButton.add(btnOpenFile);
		btnOpenFile.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
		        try {
		        	String projectPath = System.getProperty("user.dir"); // 获取项目根目录
		        	String fileName = projectPath + "/src/file.txt"; // 拼接相对路径
		            BufferedReader reader = new BufferedReader(new FileReader(fileName));// 读文件
		            String data = "";
		            String line;
		            while ((line = reader.readLine()) != null) {
		                data += line + "\n";
		            }
		            reader.close();
		            txAin.setText(data); // 设置文本框内容
		        } catch (IOException ex) {
		            ex.printStackTrace();
		        }
		    }
		});*/
		
		JButton btnClean = new JButton("CleanHTMLMark");
		btnClean.setFont(new Font("Calibri", Font.PLAIN, 18));
		panButton.add(btnClean);
		
		btnClean.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				cleanWeb();
			}
		});
		

		JButton btnCount = new JButton("CountChar");
		btnCount.setFont(new Font("Calibri", Font.PLAIN, 18));
		panButton.add(btnCount);
		btnCount.addActionListener(new ActionListener() {
		    public void actionPerformed(ActionEvent e) {
		        doCharCount(txAin.getText()); // 在点击事件中调用计算字符数方法
		    }
		});
		/*
		JButton btnSaveFile = new JButton("SaveAsFile");
		btnSaveFile.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		
		btnSaveFile.setFont(new Font("Calibri", Font.PLAIN, 18));
		panButton.add(btnSaveFile);
		*/
		
		/*JButton btnNewButton = new JButton("SaveAsFile");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});*/
		JButton btnNewButton = new JButton("SaveAsFile");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// 弹出文件选择器
				JFileChooser chooser = new JFileChooser();
				int returnVal = chooser.showSaveDialog(null); // 打开保存文件对话框
				if (returnVal == JFileChooser.APPROVE_OPTION) { // 如果用户点击了保存按钮
				    File file = chooser.getSelectedFile(); // 获取用户选择的文件
				    try {
				        BufferedWriter writer = new BufferedWriter(new FileWriter(file)); // 打开文件写入流
				        // 将需要写入文件的数据写入 writer 对象中
				        writer.write(txAin.getText());
				        writer.close(); // 关闭文件写入流
				        JOptionPane.showMessageDialog(null, "文件保存成功"); // 弹出保存成功提示框
				    } catch (IOException ex) {
				        JOptionPane.showMessageDialog(null, "文件保存失败"); // 弹出保存失败提示框
				    }
				}
			}
		});

		btnNewButton.setFont(new Font("Calibri", Font.PLAIN, 18));
		panButton.add(btnNewButton);
		
		JButton btnExit = new JButton("Exit");
		btnExit.setFont(new Font("Calibri", Font.PLAIN, 18));
		panButton.add(btnExit);
		btnExit.addActionListener(new ActionListener() {
		    @Override
		    public void actionPerformed(ActionEvent e) {
		        System.exit(0); // 直接退出应用程序
		    }
		});
		
		final JPanel panel = new JPanel();
		panel.setBounds(0, 0, 644, 37);
		panel.setBorder(new BevelBorder(BevelBorder.LOWERED));
		panel.setBackground(Color.WHITE);
		getContentPane().add(panel);
		
				final JButton btnGo = new JButton();
				btnGo.setFont(new Font("Calibri", Font.PLAIN, 18));
				btnGo.addKeyListener(new KeyAdapter() {
					public void keyReleased(final KeyEvent arg0) {
						readURL();
					}
				});
				btnGo.addActionListener(new ActionListener() {
					public void actionPerformed(final ActionEvent arg0) {
						readURL();
					}
				});
						panel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
				
						txtUrl = new JTextField();
						txtUrl.setColumns(40);
						txtUrl.setHorizontalAlignment(SwingConstants.LEFT);
						txtUrl.setText("http://www.njau.edu.cn/139/list.htm");
						txtUrl.setForeground(new Color(0, 0, 255));
						txtUrl.setFont(new Font("Arial", Font.PLAIN, 14));
						txtUrl.setName("txtUrl");
						txtUrl.setMargin(new Insets(1, 1, 0, 1));
						txtUrl.setBorder(new EtchedBorder(EtchedBorder.LOWERED));
						panel.add(txtUrl);
				btnGo.setName("btnOK");
				btnGo.setText("GO");
				panel.add(btnGo);
	
	}

	//从网站获取信息
	public void readURL() {

		try {
			URL url1 = new URL(txtUrl.getText()); // URL("http://www.njau.edu.cn");

			BufferedReader in = new BufferedReader(new InputStreamReader(url1.openStream(), "UTF-8"));

			String inputLine;
			txAin.setText("");
			while ((inputLine = in.readLine()) != null) {
				txAin.setText(txAin.getText() + inputLine);
				System.out.println(inputLine);
			}
			in.close();
			labTip.setText(txtUrl.getText());
		} catch (Exception e) {
			System.out.println("Exception thrown" + e.getMessage());
		}
	}

	// view plain copy
	// 注：这是Java正则表达式去除html标签方法。
	private static final String regEx_script = "<script[^>]*?>[\\s\\S]*?<\\/script>"; // 定义script的正则表达式
	private static final String regEx_style = "<style[^>]*?>[\\s\\S]*?<\\/style>"; // 定义style的正则表达式
	private static final String regEx_html = "<[^>]+>"; // 定义HTML标签的正则表达式
	private static final String regEx_space = "\\s*|\t|\r|\n";// 定义空格回车换行符
	private static final String regEx_w = "<w[^>]*?>[\\s\\S]*?<\\/w[^>]*?>";// 定义所有w标签
	

	//清空网页
	private void cleanWeb() {
		String webPage = new String();
		try {

			webPage = txAin.getText();
			// textArea.setText(webPage.replaceAll("<[^>]*>", ""));// 输出的字符串去除尖括号
			txAin.setText(delHTMLTag(webPage)); // .replaceAll("<[^>]*>", ""));

		} catch (Exception e) {
			System.out.println("Exception thrown");
			txAin.setText(e.getMessage());
		}
	}
	
	//统计字符
	void doCharCount(String param) {
	    if(param == null)  return;
	    HashSet<Character> hSet = new HashSet<Character>();
	    char[] cs = param.toCharArray();
	    for (char c : cs)
	        hSet.add(c);
	    ArrayList<Character> list = new ArrayList<Character>(hSet);
	    int n = hSet.size();
	    int[] times = new int[n];
	    for (char c : cs)
	        times[list.indexOf(c)] ++;

	    // 将统计结果显示在文本框中
	    StringBuilder sbResult = new StringBuilder();
	    sbResult.append("\n每个字符出现次数统计结果：\n");
	    for (int i = 0; i < n; i++) {
	        sbResult.append(String.format("%s [%d]\n", list.get(i), times[i]));
	    }
	    txAout.setText(sbResult.toString());
	}

	/**
	 * @param htmlStr
	 * @return 删除Html标签
	 */
	public static String delHTMLTag(String htmlStr) {
		Pattern p_w = Pattern.compile(regEx_w, Pattern.CASE_INSENSITIVE);
		Matcher m_w = p_w.matcher(htmlStr);
		htmlStr = m_w.replaceAll(""); // 过滤script标签

		Pattern p_script = Pattern.compile(regEx_script, Pattern.CASE_INSENSITIVE);
		Matcher m_script = p_script.matcher(htmlStr);
		htmlStr = m_script.replaceAll(""); // 过滤script标签

		Pattern p_style = Pattern.compile(regEx_style, Pattern.CASE_INSENSITIVE);
		Matcher m_style = p_style.matcher(htmlStr);
		htmlStr = m_style.replaceAll(""); // 过滤style标签

		Pattern p_html = Pattern.compile(regEx_html, Pattern.CASE_INSENSITIVE);
		Matcher m_html = p_html.matcher(htmlStr);
		htmlStr = m_html.replaceAll(""); // 过滤html标签

		Pattern p_space = Pattern.compile(regEx_space, Pattern.CASE_INSENSITIVE);
		Matcher m_space = p_space.matcher(htmlStr);
		htmlStr = m_space.replaceAll(""); // 过滤空格回车标签

		htmlStr = htmlStr.replaceAll(" ", ""); // 过滤
		return htmlStr.trim(); // 返回文本字符串
	}

}

