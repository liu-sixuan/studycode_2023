#1.假设有一段英文，其中有单独的字母I误写为i,请编写程序进行纠正。
# （思路1：）用s.split()分割字符串，得到列表，len（）判断长度，对每个列表元素进行分析。
def Func1(s1):
    words = s1.split()   #分割成单词，结果是以空格划分的一个列表
    s2 = [] #新列表，存放纠正后的字符串。
    for word in words:
        s2.append(word.replace('i', 'I'))  # 将单词中的小写字母"i"替换为大写字母"I"
    corrected_sentence = ' '.join(s2)  # 将修正后的单词列表转换回句子

    return corrected_sentence

# （思路2：）用for 遍历字符串， for x in str:, 对每个字符处理。
def Func2(s1):
    s2 = ""
    for x in s1:
        if x == 'i':
            s2 += 'I'
        else:
            s2 += x
    return s2

str = 'He is ill.iii123 '
print(Func1(str))
print(Func2(str))


