import math

def Taylor():
    x = float(input("输入度数："))
    x = x/360 * 2 * 3.1415926   #转化为弧度
    s = 1
    t = 1
    n = 1
    while abs(t)>0.000001 :
        #t 是每项。
        t = t * (-1) * (x**2) / ((2 * n - 1) * 2 * n)
        s = s + t
        n = n + 1
    print(f"值为：{s}")

Taylor()