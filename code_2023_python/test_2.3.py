#19221108-计科211-刘斯旋
#[实验10]模拟决赛现场最终成绩计算过程
#用列表存储每个评委的打分，并充分利用列表方法和内置函数
while True:
    try:
        n = int(input('请输入评委人数：'))
        assert n>2
        break
    except:
        print('必须输入大于2的整数')

scores = []

#依次输入每个评委的打分
for i in range(n):
    while True:
        try:
            score = float(input(f'请输入第{i+1}个评委的分数：'))
            assert 0<=score<=100
            scores.append(score)
            break
        except:
            print('必须属于0~100的实数。')

#去掉最高分和最低分
highest = max(scores)
lowest = min(scores)
scores.remove(highest)
scores.remove(lowest)

#计算平均分
finalscore = round(sum(scores)/len(scores),2)   #保留两位小数
print(f'去掉一个最高分{highest}\n去掉一个最低分{lowest}\n最后得分{finalscore}')


