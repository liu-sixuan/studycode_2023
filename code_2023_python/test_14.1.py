#1．编写函数，接收一个正偶数为参数，输出两个素数，并且这两个素数之和等于原来的正偶数。
# （建议求素数过程也编写一个函数）如果存在多组符合条件的素数，则全部输出。

def isPrime(num):
    if num <= 1 :
        return False
    for i in range(2,int(num**0.5) + 1):
        if num % i == 0:
            return False
    return True

def find_Primes(num):
    primes = []
    for i in range(2,num//2+1):
        if isPrime(i) and isPrime(num - i):
            primes.append((i,num-i))    #返回元组
    return primes

try:
    num = int(input("Please input your test number:"))
    if num % 2 != 0 or num <=0:
        raise ValueError("您输入的数不是正偶数！")
    primes = find_Primes(num)
    for prime in primes:
        print(prime[0], prime[1])
except ValueError as e:
    print("发生错误:", e)





