import jieba

fi = open("论语-网络版.txt", "r", encoding='utf-8')
fo = open("论语-词语统计.txt", "w", encoding='utf-8')

txt = fi.read()

# 使用jieba分词
words = jieba.lcut(txt)

# 统计词语
d = {}
for word in words:
    if word.isalnum():  # 只统计中文词语，不统计标点符号等其他字符
        d[word] = d.get(word, 0) + 1

# 写入统计结果
for word, count in d.items():
    fo.write(word + ":" + str(count) + "\n")

fi.close()
fo.close()