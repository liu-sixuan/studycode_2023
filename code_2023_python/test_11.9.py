#9. 写一个删除列表中重复元素的函数，要求去重后元素相对位置保持不变。
# 如原列表为：[1,1,2,2,3,3,’a’,’a’]，输出：[1,2,3,’a’]
# (1.可以创建一个新列表，然后加不同的元素实现)。
# (2.可以同集合去重的方式实现。

#创建一个新列表，加不同的元素
def quChong(lst):
    new_lst = []  # 创建一个新列表

    for item in lst:
        if item not in new_lst:  # 遍历原列表，如果元素不在新列表中，则添加到新列表中
            new_lst.append(item)
    return new_lst

lst = [1,1,2,2,3,3,"a","a"]
print(lst)
print(quChong(lst))