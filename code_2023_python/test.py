import pandas as pd
import csv

# 创建新数据
titles = ["新闻标题1", "新闻标题2", "新闻标题3"]
categories = ["分类1", "分类2", "分类3"]
new = pd.DataFrame({
    "新闻内容": titles,
    "新闻类别": categories
})

# 读取旧数据
old = pd.read_csv("test.csv", encoding='utf-8', engine='python')

# 更新数据集
def update(old, new):
    '''
    更新数据集：将本次新爬取的数据加入到数据集中（去除掉了重复元素）
    '''
    data = new.append(old)
    data = data.drop_duplicates()
    return data

# 打印更新数据集前的数量
print("更新数据集前数量:", old.shape[0], "条")

# 更新数据集
df = update(old, new)

# 将数据集写入 CSV 文件
df.to_csv("新闻数据集.csv", index=None, encoding='gbk')

# 打印更新后的数量和前几行数据
print("更新完毕，共有数据:", df.shape[0], "条")
print(df.head())