import random
reward = {'一等奖': (0, 0.1), '二等奖': (0.1, 0.3),
          '三等奖': (0.3, 0.6),'谢谢您': (0.6, 1)}

def rewardFun(n):
    result = {'一等奖': 0, '二等奖': 0,
          '三等奖': 0,'谢谢您': 0}
    #抽奖500次
    for i in range(n):  #抽奖n次
        num = random.random()  #生产0-1之间的随机浮点数
        for reward_,range_ in reward.items():
            if range_[0] <= num and num < range_[1]:
                result[reward_] += 1
                break
    #打印结果
    for reward_, num in result.items():
        print(f'{reward_}————————>{num}')

rewardFun(500)