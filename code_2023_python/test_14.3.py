# 输入一串字符作为密码，密码只能由数字与字母组成。编写一个函数judge(password),
# 用来求出密码的强度level,并在主程序中测试该函数，根据输入，输出对应密码强度。密码
# 强度判断准则如下（满足其中一条，密码强度增加一级）：
#①有数字：②有大写字母：③有小写字母：④位数不少于8位。

def judge(password):
    level = 0

    # 判断是否包含数字
    for char in password:
        if char.isdigit():
            level += 1
            break

    # 判断是否包含大写字母
    for char in password:
        if char.isupper():
            level += 1
            break

    # 判断是否包含小写字母
    for char in password:
        if char.islower():
            level += 1
            break

    # 判断密码长度是否大于等于8位
    if len(password) >= 8:
        level += 1

    return level

while True:
    passWrd = input("请输入测试密码(直接回车为退出):")
    if passWrd == '':
        break
    strength_level = judge(passWrd)
    print(f"{passWrd}的密码强度为{strength_level}级")
