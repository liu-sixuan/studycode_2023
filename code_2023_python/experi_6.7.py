# 7.打印金字塔。
def Print():
    n = int(input('Enter the number of lines:'))    #金字塔的行数
    for i in range(1, n + 1):   #依次打印 n 行内容
        # 打印空格
        print("  " * (n - i), end="")    #空格打完了不换行

        # 打印降序数字
        for j in range( i , 0 , -1):
            print(j, end=" ")

        # 打印升序数字
        for j in range(2, i + 1):
            print(j, end=" ")

        print()
Print()



