import csv
import re
from collections import Counter

#只统计汉字。
#想统计其他字符也可以修改
def count_chars(filename):
    with open(filename, "r", encoding="utf-8") as file:
        content = file.read().replace('\n', '')  # 去除回车
        pattern = re.compile(r'[\u4e00-\u9fa5]')  # 匹配汉字
        content = re.findall(pattern, content)  # 仅保留汉字
        counter = Counter(content)
        most_common = counter.most_common(100)
        return most_common

def save_char_count(filename, char_count):
    with open(filename, "w", encoding="utf-8", newline="") as file:
        writer = csv.writer(file)
        writer.writerow(["字符", "出现次数"])
        writer.writerows(char_count)

text_files = ["命运-网络版.txt", "寻梦-网络版.txt"]
output_files = ["命运-字符统计.csv", "寻梦-字符统计.csv"]

for i in range(len(text_files)):
    char_count = count_chars(text_files[i])
    save_char_count(output_files[i], char_count)

def find_common_chars(file1, file2, output_file):
    with open(file1, "r", encoding="utf-8") as f1, open(file2, "r", encoding="utf-8") as f2, open(output_file, "w", encoding="utf-8") as output:
        # 读取两个文件的csv格式统计结果
        reader1 = csv.reader(f1)
        reader2 = csv.reader(f2)
        # 获得字符集合并集
        char_set1 = set([row[0] for row in reader1])
        char_set2 = set([row[0] for row in reader2])
        common_chars = char_set1 & char_set2
        # 将相同字符写入新的文本文件中
        output.write(",".join(common_chars))
    print("相同字符已输出到文件中。")

find_common_chars("命运-字符统计.csv", "寻梦-字符统计.csv", "相同字符.txt")