# 在键盘上输入一段长字符串，统计其中出现的最多的字符，
# 如有多种字符出现的次数一样最多，则全部显示。
# 解题思路：遍历字符串，利用字符统计函数实现。s.count()
def charCount(s):
    tmp = dict()
    for k in s:
        if k not in tmp:
            tmp[k] = s.count(k)
    maxNum = max(tmp.values())
    for k in tmp:
        if tmp[k] == maxNum:
            print(f"['{k}']",end = ',')
    print(f"'字符出现最大次数为：{maxNum}'")

s = input("输入一串字符串：")
charCount(s)