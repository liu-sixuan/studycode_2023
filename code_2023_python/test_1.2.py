#[实验4]输入一个大于2的自然数，输出小于该数字的所有素数组成的列表。
maxNumber = int(input("请输入一个大于2的自然数："))
lst = list(range(2,maxNumber))  #创建一个大于2的自然数的列表
#最大整数的平方根
m = int(maxNumber **0.5)
#如果当前数字大于最大数的平方根，结束判断
for index,value in enumerate(lst):
    if value > m:
        break
    #使用切片对该位置之后的元素进行过滤和替换
    lst[index+1:]=filter(lambda x:x%value !=0,lst[index+1:])
print(lst)

#[实验5]输入一个大于2的自然数，输出小于该数字的所有素数组成的集合
maxNumber = int(input("请输入一个大于2的自然数："))
numbers = set(range(2,maxNumber)) #numbers是所有小于该数字的自然数的集合
m = int (maxNumber ** 0.5)+1  #m是要找的最大因子
#下面要打印一个 2-m范围内的素数的列表
primesLessThanM = [p for p in range(2,m) #p是2-m的是所有自然数
                   #依次判断这些数是不是素数
                   if 0 not in [p%d  #p%d！=0的时候是素数。
                                for d in range(2,int(p**0.5)+1)]]
#遍历最大正数平方根之内的所有自然数
#根据列表，从集合中剔除非素数
for p in primesLessThanM:
    for i in range(2,maxNumber//p+1):   #除去所有素数的倍数
        numbers.discard(i*p)

print(numbers)

#[实验6]
#使用random 和列表推导式生成一个包含50个介于1-100的随机整数的列表。
# 编写函数def isPrime(n)测试整数n是否为素数。
#使用内置函数filter（）把函数isPrime()作用到包含若干随机整数的列表lst上，最后程序输出一个列表，其中只包含列表lst中不是素数的那些整数

import random

def isPrime(n):
    #2是最小的素数
    if n in (2,3):
        return True
    #去掉其他偶数
    if n%2==0:
        return False
    for i in range(3,int(n**0.5)+1,2):
        if n % i == 0:
            return False
    return True

lst = []
for i in range(1,51):
    lst.append(random.randint(1,100))
print(lst)
print(list(filter(isPrime,lst)))

#[实验7]
#分析代码功能。发现并解决错误
#这段代码试图计算组合数Cni，但是由于浮点数除法时精度问题导致结果错误
def cni(n,i):
    minNI = min(i,n-i)
    result=1
    for j in range(0,minNI):
        result=result*(n-j)//(minNI-j)  #改成整除就好了
    return result

