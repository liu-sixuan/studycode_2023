# 3.某高校举办英语口语比赛，20个评委对参赛选手打分，用input输入（打分范围是1 - 10之间的实型数据），
# 计算总分时，需要去掉一个最高分，去掉一个最低分，然后输出选手的平均得分，请编写程序实现。
def Aver():
    print('评委请开始打分：')
    a = []  #创建一个空的列表
    for i in range(20):
        while True:
            try:    #1-10之间，实数
                score = float(input(f'请输入评委{i+1}的评分：'))
                assert 1 <= score <= 10 #1-10之间的实数
                a.append(score)
                break
            except:
                print("请输入1-10之间的实数")
    maxNum = max(a)
    minNum = min(a)
    a.remove(maxNum)
    a.remove(minNum)

    #计算平均分
    aver = sum(a)/len(a)
    print(f'去掉最大和最小分后，选手的平均分是：{aver}')
    return aver

Aver()

