#请编写程序，对这个《论语》文本中出现的汉字和标点符号进行统计，
# 字符与出现次数之间用冒号:分隔，
# 输出保存到“论语-汉字统计.txt”文件中，

fi = open("论语-网络版.txt", "r", encoding='utf-8')
fo = open("论语-汉字统计.txt", "w", encoding='utf-8')

txt = fi.read()

# 统计汉字和标点符号
d = {}
for c in txt:
    if c != ' ' and c != '\n':
        d[c] = d.get(c, 0) + 1

# 写入统计结果
for c, count in d.items():
    fo.write(c + ":" + str(count) + "\n")

fi.close()
fo.close()