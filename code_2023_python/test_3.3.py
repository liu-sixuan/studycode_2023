#模拟蒙蒂霍尔悖论游戏

from random import randrange

#随机生成三个门。一个门后是车，两个门后是羊。所以其实只要确认车的位置就可以了。
def init():
    #在Python中，初始化字典时可以仅仅指定键而不指定对应的值。
    #先创建一个字典，里面三个都是羊
    result = {i:'goat' for i in range(3)}
    #result = { i:'goat' for i in range(3)}
    r = randrange(3)    #下标是0，1,2三个，所以randrange(3)
    result [r] = 'car'; #随机令一个门后是车
    return result

def startGame():
    #获取游戏中，门的情况。
    doors = init()
    #玩家选择门号
    while True:
        try:    #防止玩家说出不存在的门号
            firstDoorNum = int(input('Choose a door to open:'))
            assert 0 <= firstDoorNum <= 2
            break
        except:
            print('Door number must be between 0 and 2.')
    #主持人查看 除了玩家选的门之外 后面是山羊 的门。
    for door in doors.keys()-{firstDoorNum}:
        if doors[door] == 'goat':
            print(f"'goat' behind the {door} door")
            break
    thirdDoorNum = (doors.keys()-{door,firstDoorNum}).pop()
    change = input(f'Switch to {thirdDoorNum}?(y/n) ')
    if change == 'y':   #换门
        finalDoorNum = thirdDoorNum
    else:
        finalDoorNum = firstDoorNum
    if doors[finalDoorNum] == 'goat':   #选中羊
        return 'I win!'
    else:
        return 'You win!'

while True:
    print('= '*30)  #分割线
    print(startGame())
    r = input('Do you want to try once more?(y/n)')
    if r == 'n':
        break   #游戏结束

