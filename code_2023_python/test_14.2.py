#编写函数，实现提取英文短语的首字母，返回大写形式。

def get_initials(phrase):
    initials = ""
    for word in phrase.split(): #一个单词一个单词地找
        initials += word[0].upper() #大写首字母。
    return initials

phrase = input("输入短语：") #输入短语
ini = get_initials(phrase)
print (ini)