# 5.求两个数的最大公约数。设两数a、b(a≥b), 求a和b的最大公约数(a, b)
def gcd(a, b):
    while b != 0:
        a, b = b, a % b
    return a

num1 = int(input("请输入第一个数："))
num2 = int(input("请输入第二个数："))

result = gcd(num1, num2)
print(f"最大公约数是{result}")

