#二、阈值与平滑处理
# 1.阈值(灰度图)
# 2.图像平滑
import cv2
import matplotlib
matplotlib.use('TkAgg') #将 matplotlib 的后端设置为 GUI 后端
import matplotlib.pyplot as plt
import numpy as np


# 1.阈值(灰度图)
# 这段代码的作用是使用 OpenCV 和 Matplotlib 库处理一张图像，
# 并显示原始图像和不同的阈值处理结果。
img = cv2.imread('lenna.bmp')   #读取图像（原图）
cv2.imshow("lenna",img)
cv2.waitKey(0)
#转化为灰度图
img_gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
#显示灰度图
cv2.imshow("gray",img_gray)
cv2.waitKey(0)

#使用不同的阈值方法对灰度图像进行处理：# 每种方法都会生成一张处理后的图像。
#这里使用了不同的阈值方法，
# 1.二值化（BINARY）：将图像分为两个部分，
# 大于阈值的像素值设为一个固定的最大值（在代码中为255），小于阈值的像素值设为0。
ret, thresh1 = cv2.threshold(img_gray, 127, 255, cv2.THRESH_BINARY)
# 2.反二值化（BINARY_INV）
#反二值化是二值化的逆过程，它将图像分为两个部分，
# 大于阈值的像素值设为0，小于阈值的像素值设为一个固定的最大值（在代码中为255）。
ret, thresh2 = cv2.threshold(img_gray, 127, 255, cv2.THRESH_BINARY_INV)
# 截断（TRUNC）
#将图像中大于阈值的像素值设为阈值，小于阈值的像素值保持不变。
ret, thresh3 = cv2.threshold(img_gray, 127, 255, cv2.THRESH_TRUNC)
# 零化（TOZERO）
#将图像中小于阈值的像素值设为0，大于等于阈值的像素值保持不变。
ret, thresh4 = cv2.threshold(img_gray, 127, 255, cv2.THRESH_TOZERO)
# 反零化（TOZERO_INV）
#将图像中大于阈值的像素值设为0，小于等于阈值的像素值保持不变。
ret, thresh5 = cv2.threshold(img_gray, 127, 255, cv2.THRESH_TOZERO_INV)

#准备显示的标题和对应的图像：
titles = ['Original Image', 'BINARY', 'BINARY_INV',
          'TRUNC', 'TOZERO', 'TOZERO_INV']
images = [img, thresh1, thresh2, thresh3, thresh4, thresh5]

for i in range(6):
    #使用 Matplotlib 在一个 2x3 的子图网格中显示原始图像和处理后的图像：
    plt.subplot(2, 3, i + 1)
    # 然后使用plt.imshow()显示每个子图中的图像，使用灰度色彩映射'gray'，
    plt.imshow(images[i], 'gray')
    # plt.title()：设置每个子图的标题。最后，使用
    plt.title(titles[i])
    #隐藏 Matplotlib 子图中的 X 轴和 Y 轴刻度标签的函数调用。
    #plt.xticks([]) 是用来隐藏 X 轴刻度标签，plt.yticks([]) 是用来隐藏 Y 轴刻度标签。
    #通过将空列表 [] 作为参数传递，这些函数将移除对应轴的刻度标签，
    # 并使子图中不显示刻度标签。
    plt.xticks([]), plt.yticks([])
# plt.show()
# 显示所有子图。
plt.show()


#2.图像平滑
# 这段代码展示了不同的图像平滑方法，
# 包括均值滤波、方框滤波、高斯滤波和中值滤波。
# 代码中使用了 OpenCV 库中的相应函数来实现这些操作。

# 均值滤波
# 简单的平均卷积操作（卷积核：9个全1的矩阵）

# 使用 cv2.blur() 函数对原始图像 img 进行均值滤波处理。
# 均值滤波是简单的平均卷积操作，即使用一个卷积核（在这里是一个 3x3 的全1矩阵）
# 对图像进行卷积。这个卷积核会计算每个像素周围邻域像素的平均值，
# 并将结果作为该像素的新值。均值滤波对图像进行平滑处理。

blur = cv2.blur(img, (3, 3))
cv2.imshow('blur', blur)
cv2.waitKey(0)
# 方框滤波
# 基本和均值一样，可以选择归一化,容易越界
# 使用 cv2.boxFilter() 函数对原始图像 img 进行方框滤波处理。
# 与均值滤波类似，方框滤波也是一种平均滤波方法。
# 卷积核的值为 1，可以选择进行归一化操作。
# 方框滤波计算卷积核覆盖区域内的像素值平均值，并将结果作为中心像素的新值。
box = cv2.boxFilter(img,-1,(3,3), normalize=False)
cv2.imshow('box', box)
cv2.waitKey(0)
# 高斯滤波
# 高斯模糊的卷积核里的数值是满足高斯分布，相当于更重视中间的
# 使用 cv2.GaussianBlur() 函数对原始图像 img 进行高斯滤波处理。
# 高斯滤波使用满足高斯分布的卷积核对图像进行卷积处理。
# 这个卷积核的数值更重视中间的像素，通过卷积操作实现图像的平滑。
# 第二个参数 (5, 5) 表示高斯核的大小为 5x5，第三个参数 1 表示高斯核的标准差。
aussian = cv2.GaussianBlur(img, (5, 5), 1)
cv2.imshow('aussian', aussian)
cv2.waitKey(0)
# 中值滤波
# 相当于用中间值代替，平滑(去噪)效果最好
# 使用 cv2.medianBlur() 函数对原始图像 img 进行中值滤波处理。
# 中值滤波使用一个有序像素集合中的中间值来代替中心像素值，
# 能够有效地去除图像中的噪声，并保持边缘的细节。

median = cv2.medianBlur(img, 5)
cv2.imshow('median', median)

plt.show()
cv2.waitKey(0)
cv2.destroyAllWindows()


