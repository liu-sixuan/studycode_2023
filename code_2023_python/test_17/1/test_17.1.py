# 读取学生名单
with open('../2/Name.txt', 'r', encoding='utf-8') as f:
    student_names = f.readlines()
student_names = [name.strip() for name in student_names]

# 读取第一次考勤数据
with open('cc.csv', 'r', encoding='utf-8') as f:
    attendance_data = f.readlines()
attendance_data = [data.strip().split(',') for data in attendance_data]

# 创建一个字典记录每个学生的考勤情况
attendance_dict = {}
for data in attendance_data:
    student = data[0]
    attendance = data[1]
    attendance_dict[student] = attendance

# 统计第一次缺勤同学的名单
absent_students = []
for student in student_names:
    if student not in attendance_dict or attendance_dict[student] == '缺勤':
        absent_students.append(student)

# 打印第一次缺勤同学的名单
print("第一次缺勤同学有：",end = "")
for student in absent_students:
    print(student,end = " ")