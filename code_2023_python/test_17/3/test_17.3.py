# 打开文件并读取数据
with open('data.txt', 'r') as file:
    # 读取所有行
    lines = file.readlines()

# 初始化一个空列表用于存放每行的总和和平均值
line_sums = []
line_averages = []

# 跳过可能的文本标题行
for line in lines:
    try:
        # 尝试提取每行的数字部分
        numbers = [int(item.split(':')[1]) for item in line.strip().split(',')]
        line_sum = sum(numbers)
        line_average = round(line_sum / len(numbers), 2)  # 保留两位小数

        # 添加每行的总和和平均值到相应的列表
        line_sums.append(line_sum)
        line_averages.append(line_average)
    except (ValueError, IndexError):
        # 如果提取失败，说明该行不符合格式，可以忽略
        pass

# 输出每行的总和和平均值
for i, (line_sum, line_average) in enumerate(zip(line_sums, line_averages), start=1):
    print(f"总和是: {line_sum}, 平均值是: {line_average}")

