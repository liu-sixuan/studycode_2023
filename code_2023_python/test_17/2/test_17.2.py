import csv

# 读取班级名单
with open('Name.txt', 'r',encoding = "utf-8") as name_file:
    student_names = name_file.read().splitlines()

# 初始化考勤记录字典，键为学生姓名，值为出勤次数
attendance_record = {name: 0 for name in student_names}

# 遍历考勤文件，更新考勤记录
for i in range(1, 11):  # 假设有10次考勤
    file_name = f'{i}.csv'
    with open(file_name, 'r',encoding = "utf-8") as csv_file:
        reader = csv.reader(csv_file)
        for row in reader:
            student_name = row[0]
            # 如果该学生在名单中，增加考勤次数
            if student_name in attendance_record:
                attendance_record[student_name] += 1

# 输出全勤同学的名字
full_attendance_students = [name for name, count in attendance_record.items() if count == 10]
print("全勤学生有：",end = "")
for student in full_attendance_students:
    print(student,end = ",")






