class Box:
    def __init__(self, length, width, height):
        self.length = length
        self.width = width
        self.height = height

    def volume(self):
        return self.length * self.width * self.height

    def superficial(self):
        return 2 * (self.length * self.width + self.length * self.height + self.width * self.height)

# 创建一个实例对象，计算体积和表面积
b1 = Box(1, 1, 1)  # 长、宽、高
print("体积:", b1.volume())  # 输出体积
print("表面积:", b1.superficial())  # 输出表面积