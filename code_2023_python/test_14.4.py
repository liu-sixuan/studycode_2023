def fun(n):
    sum = 0
    for i in range(1,n+1):
        sum += 1/i
    return sum

print(fun(1))
print(fun(2))
print(fun(10))
print(fun(100))
print(fun(500))

