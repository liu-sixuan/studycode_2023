import csv

filename = "SunSign.csv"

# 读取CSV文件内容并保存为字典
sunsigns_dict = {}
with open(filename, newline='', encoding='utf-8-sig') as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        start_month, start_day = int(row['开始月日'][:1]), int(row['开始月日'][1:])
        end_month, end_day = int(row['结束月日'][:1]), int(row['结束月日'][1:])
        sunsigns_dict[row['星座']] = {"开始月日": (start_month, start_day), "结束月日": (end_month, end_day), "Unicode": chr(int(row['Unicode'], 16))}
while True:
    # 循环获得用户输入
    sunsign = input("请输入星座名称（输入exit退出）：")
    if sunsign == "exit":
        break

    if sunsign in sunsigns_dict:
        # 如果星座名称在字典中，输出信息
        info = sunsigns_dict[sunsign]
        start_month, start_day = info['开始月日']
        end_month, end_day = info['结束月日']
        print("{}星座出生范围为{}月{}日至{}月{}日，对应字符为{}".format(sunsign, start_month, start_day, end_month, end_day, info['Unicode']))
    else:
        # 如果星座名称不在字典中，输出错误信息
        print("输入星座名称有误！")