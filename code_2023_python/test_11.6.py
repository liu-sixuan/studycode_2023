#6.输入一个人民币的整数值（100以内以元为单位），
# 编程找到用10元、5元、2元、1元表示的总数量的最小组合方式。
def minZuhe(n):
    RMB = {'10元': 0, '5元': 0,
              '2元': 0, '1元': 0}
    tmp = n // 10
    RMB["10元"] += tmp
    n -= 10 * tmp
    tmp = n//5
    RMB["5元"] += tmp
    n -= 5 * tmp
    tmp = n // 2
    RMB["2元"] += tmp
    n -= 2 * tmp
    RMB["1元"] += n
    return RMB
num = int(input("请输入一个100以内的人民币整数值："))
dictRMB = minZuhe(num)
print("最小组合方式为：")
for i,j in dictRMB.items():
    print(f"{i}:{j}张")