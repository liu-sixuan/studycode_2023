#编写一个函数，输入n为偶数时，调用函数求1/22+1/42+…+1/n2,
# 当输入n为奇数时，调用函数1/12+1/32+…+1/n2的值。
def Sum(n):
    if n == 1 or n == 2:
        return 1/(n**2)
    else:
        return 1/(n**2)+Sum(n-2)
def Fun(n):
    if n % 2 == 0 :
        print("输入的为偶数。")
    elif n%2 == 1:
        print("输入的为奇数。")

    return Sum(n)
num = int(input('请输入一个数：'))
print(Fun(num))