#八、答题卡判卷

import numpy as np
import argparse
import imutils
import cv2
import pandas as pd
from imutils import contours
def cv_show(name,img):
    cv2.imshow(name,img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

def four_point_transform(image, pts):
    # 获取输入坐标点
    rect = order_points(pts)
    (tl, tr, br, bl) = rect

    # 计算输入的w和h值
    widthA = np.sqrt(((br[0] - bl[0]) ** 2) + ((br[1] - bl[1]) ** 2))
    widthB = np.sqrt(((tr[0] - tl[0]) ** 2) + ((tr[1] - tl[1]) ** 2))
    maxWidth = max(int(widthA), int(widthB))

    heightA = np.sqrt(((tr[0] - br[0]) ** 2) + ((tr[1] - br[1]) ** 2))
    heightB = np.sqrt(((tl[0] - bl[0]) ** 2) + ((tl[1] - bl[1]) ** 2))
    maxHeight = max(int(heightA), int(heightB))

    # 变换后对应坐标位置
    dst = np.array([
        [0, 0],
        [maxWidth - 1, 0],
        [maxWidth - 1, maxHeight - 1],
        [0, maxHeight - 1]], dtype="float32")

    # 计算变换矩阵
    M = cv2.getPerspectiveTransform(rect, dst)
    warped = cv2.warpPerspective(image, M, (maxWidth, maxHeight))

    # 返回变换后结果
    return warped
#

# data = pd.read_csv("test.csv")
# --------------------------0.正确答案----------------------------------
# 定义了一个字典 ANSWER_KEY，其中存储了问题的正确答案。
# 该字典的键是问题的索引，值是对应问题的正确答案。
ANSWER_KEY = {0: 1, 1: 4, 2: 0, 3: 3, 4: 1}

# ---------------------------1.预处理-----------------------------------
#读取图像（test.png)并显示出来
image = cv2.imread("test.png")
#cv_show("Initial_img",image)
# 将图像进行拷贝并转化为灰度图像
contours_img = image.copy() #拷贝图像存在contours_img中，以便后面绘制轮廓后显示。
gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)  #灰度化图像
#应用了高斯模糊来平滑图像。使用 cv2.GaussianBlur() 函数对灰度图像进行处理，
# 第二个参数是高斯核的大小，第三个参数是高斯函数的标准差。
blurred = cv2.GaussianBlur(gray, (5, 5), 0)	#对灰度图进行高斯模糊。
cv_show('blurred',blurred)

#使用 Canny 边缘检测算子检测边缘。
# 前两个参数是边缘检测算子的阈值，第一个参数是最小阈值，第二个参数是最大阈值。
#这一步为后续提取轮廓做准备的。
edged = cv2.Canny(blurred, 50,100 ) #50-100最好
cv_show('edged',edged)

# ----------------------------2.轮廓检测-----------------------------
#找到图像中的轮廓
#edged.copy() 表示传递给函数的输入图像副本，
# cv2.RETR_EXTERNAL 指定了轮廓检测的模式（只检测最外层的轮廓），
# cv2.CHAIN_APPROX_SIMPLE 指定了轮廓的近似方法。
# [0] 表示返回的结果中的第一个元素，即轮廓列表。所以，cnts 变量将保存找到的轮廓。
cnts = cv2.findContours(edged.copy(), cv2.RETR_EXTERNAL,
	cv2.CHAIN_APPROX_SIMPLE)[0]
#绘制轮廓
#cnts 是轮廓列表，-1 表示绘制所有的轮廓，
# (0, 0, 255) 是绘制轮廓的颜色（蓝色），3 是绘制轮廓的线宽。
cv2.drawContours(contours_img,cnts,-1,(0,0,255),3)
cv_show('contours_img',contours_img)
docCnt = None

# # ----------------------2.提取最大轮廓，做近似---------------------------
# 做近似

# if len(cnts) > 0:
# 	# 根据轮廓大小进行排序
#     cnts = sorted(cnts, key=cv2.contourArea, reverse=True)
#     docCnt = cnts[0]  # 最大轮廓
# peri = cv2.arcLength(docCnt, True)
# approx = cv2.approxPolyDP(docCnt, 0.02 * peri, True)
	# # 遍历每一个轮廓
	# for c in cnts:
	# 	# 近似
	# 	peri = cv2.arcLength(c, True) #计算轮廓周长
	# 	approx = cv2.approxPolyDP(c, 0.02 * peri, True) #以指定的精度近似轮廓。
    #     # 第一个参数是轮廓的点集，第二个参数是精度，
	# 	# 即近似线段与原始轮廓之间的最大偏差。第三个参数表示是否闭合轮廓。
	# 	# 准备做透视变换
	# 	if len(approx) == 4:    #判断近似轮廓的边数是否等于4，如果是，则找到轮廓，
	# 		docCnt = approx
	# 		break

#     # ------------------------------3.执行透视变换-------------------------------
# #透视变换需要原始图像和矩形的四个顶点坐标作为输入。
# # 该函数将扭曲的图像保存在变量 warped 中。
# warped = four_point_transform(gray, docCnt.reshape(4, 2))
# cv_show('warped', warped)
# # 自适应，二值图
# #cv2.threshold 函数对图像进行自动阈值化处理，参数选择合适的阈值化方法。
# thresh = cv2.threshold(warped, 0, 255,
#                        cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU)[1]  # 0 代表自适应阈值
# cv_show('thresh', thresh)
# #上述代码首先对扭曲后的图像进行二值化处理，即将图像转换为黑白图像，并使轮廓变为白色填充。
# 透视变换
# 使用透视变换将图像变为矩形，方便后续计算圆圈轮廓的位置
# 定义标准的映射坐标，用于透视变换
pts = approx.reshape(4, 2)
rect = np.zeros((4, 2), dtype="float32")

# 计算矩形的边界框
# 左上，右上，右下，左下
s = pts.sum(axis=1)
rect[0] = pts[np.argmin(s)]
rect[2] = pts[np.argmax(s)]

diff = np.diff(pts, axis=1)
rect[1] = pts[np.argmin(diff)]
rect[3] = pts[np.argmax(diff)]

# 计算新图像的尺寸
(tl, tr, br, bl) = rect
widthA = np.sqrt((br[0] - bl[0]) ** 2 + (br[1] - bl[1]) ** 2)
widthB = np.sqrt((tr[0] - tl[0]) ** 2 + (tr[1] - tl[1]) ** 2)
maxWidth = max(int(widthA), int(widthB))

heightA = np.sqrt((tr[0] - br[0]) ** 2 + (tr[1] - br[1]) ** 2)
heightB = np.sqrt((tl[0] - bl[0]) ** 2 + (tl[1] - bl[1]) ** 2)
maxHeight = max(int(heightA), int(heightB))

# 定义透视变换的目标点坐标，将图像映射为矩形
dst = np.array([[0, 0], [maxWidth - 1, 0], [maxWidth - 1, maxHeight - 1], [0, maxHeight - 1]], dtype="float32")

# 进行透视变换
M = cv2.getPerspectiveTransform(rect, dst)
warped = cv2.warpPerspective(gray, M, (maxWidth, maxHeight))

cv_show('Warped', warped)

# 对透视变换后的图像进行阈值处理，得到二值图像
thresh = cv2.threshold(warped, 0, 255, cv2.THRESH_BINARY_INV | cv2.THRESH_OTSU)[1]


# ----------------------------4.找到所有圆圈轮廓（删除其他轮廓）-----------------------------
thresh_Contours = thresh.copy()                           # thresh是二值化的答题卡

cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL,
	cv2.CHAIN_APPROX_SIMPLE)[1]
if cnts:
    cv2.drawContours(thresh_Contours,cnts,-1,(0,0,255),3)
cv_show('thresh_Contours',thresh_Contours)
questionCnts = []

# 遍历
for c in cnts:
	# 计算比例和大小
	(x, y, w, h) = cv2.boundingRect(c)
	ar = w / float(h)

	# 根据实际情况指定标准
	if w >= 20 and h >= 20 and ar >= 0.9 and ar <= 1.1:
		questionCnts.append(c)

# # # ----------------------------4.找到所有圆圈轮廓（删除其他轮廓）-----------------------------
# thresh_Contours = thresh.copy()                           # thresh是二值化的答题卡
# cnts = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL,
# 	cv2.CHAIN_APPROX_SIMPLE)[0]
# cv2.drawContours(thresh_Contours,cnts,-1,(0,0,255),3)
# cv2.imshow('thresh_Contours',thresh_Contours)
# questionCnts = [] #找到轮廓，并用一个列表 questionCnts 存储题目轮廓。
#
# # 遍历
# for c in cnts:
# 	# 计算比例和大小
# 	(x, y, w, h) = cv2.boundingRect(c)
# 	ar = w / float(h)
#
# 	# 根据实际情况指定标准
# 	if w >= 20 and h >= 20 and ar >= 0.9 and ar <= 1.1:
# 		questionCnts.append(c)
#
# questionCnts = contours.sort_contours(questionCnts, method="top-to-bottom")[0]
# correct = 0
#
# for (q, i) in enumerate(np.arange(0, len(questionCnts), 5)):
#     cnts = contours.sort_contours(questionCnts[i:i + 5])[0]
#     bubbled = None
#
#     for (j, c) in enumerate(cnts):
#         mask = np.zeros(thresh.shape, dtype="uint8")
#         cv2.drawContours(mask, [c], -1, 255, -1)
#
#         # 计算轮廓中白色像素的个数
#         mask = cv2.bitwise_and(thresh, thresh, mask=mask)
#         total = cv2.countNonZero(mask)
#
#         if bubbled is None or total > bubbled[0]:
#             bubbled = (total, j)
#
#     color = (0, 0, 255)
#     k = ANSWER_KEY[q]
#
#     # 判断答案是否正确
#     if k == bubbled[1]:
#         color = (0, 255, 0)
#         correct += 1
#
#     cv2.drawContours(warped, [cnts[k]], -1, color, 3)

