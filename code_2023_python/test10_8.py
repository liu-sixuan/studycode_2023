#编写函数，可以接收任意多个整数并输出其中的最大值和所有整数之和。（采用可变长度参数）
def max_and_sum(*args):
    max_num = float('-inf')  # 设定一个负无穷大作为初始最大值
    total_sum = 0
    for num in args:
        if num > max_num:
            max_num = num
        total_sum += num
    return max_num, total_sum

#可以是任意多个数字做参数
result = max_and_sum(10, 20, 40, 50)
max_value, sum_value = result
print("最大值:", max_value)
print("总和:", sum_value)