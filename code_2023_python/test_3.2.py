#实验十三，模拟轮盘抽奖游戏
#0-30--一等奖。30-108--二等奖。108-360--三等奖。（左闭右开）
#试玩10000次，记录每个奖项的中奖次数。

from random import randrange

def playGame():
    #生成一个0-360之间的随机整数（默认是整数）
    value = randrange(360) #randrange(start,stop,step)
    #看本次获奖情况。
    for k,v in areas.items():
        if v[0] <= value <v [1]:
            return k

areas = {'一等奖':[0,30],
         '二等奖':[30,108],
         '三等奖':[108,360]}   #这里的值应该是区间最值的列表。书上的答案是错的。

results = {'一等奖':0,
           '二等奖':0,
           '三等奖':0}    #存储各个等级奖项的中奖次数

#试玩 10000 次
for i in range(10000):
    r = playGame()  #r存储返回的键值。
    #get()根据括号中的键，获得其值。
    results[r] = results.get(r) + 1 #这里results[r]和results.get(r)其实是等效的。

for key in results:
    print(key,results[key])