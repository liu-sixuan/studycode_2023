#开发敏感词语过滤程序，提示用户输入内容，如果用户输入的内容中包含特殊的字符：
# 如 "枪支弹药"  "借贷平台"  则将内容替换为***
#解题思路：用find方法来判断用户输入的字符串里面是否有敏感词，有的话find的值就不是-1.
def sensitiveWords(s1):
    senWords = ['枪支弹药','借贷平台']
    for k in senWords:
        if s1.find(k) != -1:
            s1 = s1.replace(k,"***")
    return s1

s = input("请输入字符串：")
print(sensitiveWords(s))