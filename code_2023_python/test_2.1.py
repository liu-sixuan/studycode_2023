#19221108-计科211-刘斯旋
#[实验8]枚举法验证6174猜想
from string import digits
from itertools import combinations

for item in combinations(digits,4):     #4位数字的全组合。但是这个时候是字符串
    times = 0   #相减次数初始值为0
    #dif = int(item) #初始化
    while True:
        #当前选择的4个数字的组合中，最大和最小的数字
        max = int(''.join(sorted(item,reverse = True)))
        min = int(''.join(sorted(item)))
        dif = max - min     #做一次差
        times = times + 1
        #如果差值是6174 退出循环。否则循环继续。如果次数大于7次则失败，否则打印次数。
        if dif == 6174:
            if times > 7:
                print(f'6174猜想是错误的，找到一个反例组合{item}.该组合用了{times}次作差')
            break  # 本次组合满足要求
        else:  # 不满足，继续作差
            item = str(dif)  # 用差继续作差

