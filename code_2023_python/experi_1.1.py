#编写程序，检测输入的字符串中中英文字母、空格、数字和其它字符的个数。
#解题思路：利用好 while 或 for 语句,
# 采用python字符串内建函数isspace()，isdigit()，isalpha()实现。
# 判断时先获取字符串长度，然后采用遍历方法检测每一个字符并归类。
def Count(str):
    numS = 0;numD = 0;numA = 0;numElse = 0
    for char in str:
        if(char.isspace()):
            numS = numS + 1
        elif(char.isdigit()):
            numD = numD + 1
        elif(char.isalpha()):
            numA = numA + 1
        else:
            numElse = numElse +1
    print(f'空格有：{numS}')
    print(f'数字有：{numD}')
    print(f'英文字母有：{numA}')
    print(f'其他字符有：{numElse}')

str = input("请输入字符串：")
Count(str)
