#1、请编写程序，生成随机密码。具体要求如下：
# （1）使用 random 库，采用 0x1010 作为随机数种子。
# （2）密码 
# abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%^&* 中的字符组成。
# （3）每个密码长度固定为 10 个字符。
# （4）程序运行每次产生 10 个密码，每个密码一行。
# （5）每次产生的 10 个密码首字符不能一样。
# （6）程序运行后产生的密码保存在“随机密码.txt”文件中。

import random

#生成随机密码的函数
def generate_password():
    chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!@#$%^&*'
    #random.sample() 函数从 chars 中随机选择10个字符，并将它们连接起来形成密码。
    password = ''.join(random.sample(chars, 10))
    return password

#random.seed() 函数将种子设置为十六进制数 0x1010。
# 这样每次运行程序时，都会使用相同的种子，从而确保生成的随机数序列是相同的。
#（设置随机种子的话，每次结果都是一样的。）
random.seed(0x1010)

#passwords是一个空集合，用来存储所有的密码。
passwords = set()
#存储10条密码
#由于集合的特性，重复的密码将被自动过滤掉。
while len(passwords) < 10:
    passwords.add(generate_password())

#我们使用 open() 函数以写入模式打开名为 “随机密码.txt” 的文件。
#使用一个循环将生成的密码逐行写入文件中。
with open('随机密码.txt', 'w') as file:
    for password in passwords:
        file.write(password + '\n')

print("随机密码已生成并保存到“随机密码.txt”文件中。")