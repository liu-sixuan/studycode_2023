#求s=a+aa+aaa+aaaa+aa…a的值，
# 其中a是一个数字，用键盘输入，几个数相加也由键盘输,
# 例如1+11+111+1111 (此时共有4个数相加)，
#解题思路：用字符串解决数字重复累加问题，
# 即将数字转化为字符串实现重叠，然后将重叠后的字符串转化为数字在计算。
def Sum():
    num = int(input("被加的数字："))
    Cishu = int(input("加几次？："))
    sum = 0
    tmp = num
    for i in range(Cishu):
        sum = sum + tmp
        tmp = tmp * 10 + num
    print(f"结果是：{sum}")
    
Sum()