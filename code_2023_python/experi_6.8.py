# 8.用户输入若干个分数，求所有分数的平均分。每输入一个分数后询问是否继续输入下一个分数，
# 回答“yes”就继续输入下一个分数，回答“no”就停止输入分数。

def AverScores():
    scores = []
    while True:
        score = float(input("请输入一个分数："))
        scores.append(score)
        answer = input("是否继续输入下一个分数？（回答'yes'或'no'）")
        if answer.lower() == "no":
            break

    average_score = sum(scores) / len(scores)
    print(f"所有分数的平均分为：{average_score}")

AverScores()