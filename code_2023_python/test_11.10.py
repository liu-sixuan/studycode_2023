# 10.已知道有一系统，包括多个用户，用户的信息保存在列表里面，每个用户都有自己对应的登陆密码，
# 模拟用户登陆，密码和用户名能对应则表明登陆成功。
# 解题思路：
# （1）判断用户是否存在
# （2）如果存在，则
#         判断用户密码是否正确
#         如果正确，则登陆成功。
#         如果密码不正确，则重新登陆，共3次机会。
# （3）用户不存在
# 重新登陆，3次机会。
def System():
    Users=["张三","李四","王五","赵六"]
    Passwords=["123","456","abcd","5try"]
    for i in range(3):
        user = input("用户名:")
        if user in Users:
            pwd = input("密码:")
            if pwd == Passwords[Users.index(user)]:
                print(f"{user}登录成功！")
                return True
            else:
                print(f"密码错误，还剩{2 - i}次机会")
                continue
        else:
            print(f"用户不存在，还剩{2 - i}次机会")
    return False

System()