#19221108-计科211-刘斯旋
#[实验9]计算小明爬楼梯的爬法数量
#递推法（循环法）
def climbStairs1(n):
    a=[1,2,4] #从底下三层往上爬，一级级递推
    for i in range(n-3):
        #上第四个台阶，可以从第一个、第二个、第三个台阶上去。
        # 所以上第四个台阶的方法数是前三个方法的总和。以此类推除了头三个台阶外的所有台阶
        #[方法一] a.append(a[i] + a[i+1] + a[i+2])
        a[2],a[1],a[0]=sum(a),a[2],a[1]
    return a[-1]

#递归法
def climbStairs2(n):
    first3={1:1,2:2,3:4}    #从最后一级台阶倒着往前推，1.2.3阶台阶分别有1/2/4种上法（结束条件）
    if n in first3:
        return first3[n]    #递归结束条件:到达头三个台阶
    else:   #其余每个台阶有三种爬上来的方法，倒着推
        return climbStairs2(n-1) + climbStairs2(n-2) + climbStairs2(n-3)

print(climbStairs1(15))
print(climbStairs2(15))