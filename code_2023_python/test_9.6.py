# 已知一数列特点如下，打印出第20个数。
# 数列特点：F1 =1     (n=0)
# F2 = 1    (n=1)
# Fn = 1+F[n-1]+ F[n-2] (n=>2)  递归
def f(n):
    if (n == 1) or (n == 0):
        return 1
    return 1+f(n-1)+f(n-2)
print(f(20))