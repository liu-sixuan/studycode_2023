# 3、s = "ajldjlajfdljfddd"，去重并从小到大排序输出"adfjl"
# (set去重，去重转成list,利用sort方法排序，reverse=False是从小到大排
# list是可变数据类型，s.sort时候没有返回值)
s = "ajldjlajfdljfddd"
s_set = set(s)
s_sort = sorted(list(s_set),reverse = False)
result = "".join(s_sort)
print(result)