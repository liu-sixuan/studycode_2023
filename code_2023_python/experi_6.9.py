# 9.输出所有3位“水仙花数”。所谓n位水仙花数是指1个n位的十进制数，其各位数字的n次方之和等于该数本身。例如：153
# 是水仙花数，因为153 = 13 + 53 + 33 。
def ArmstrongNum3():
    for num in range(100, 1000):
        digit1 = num // 100
        digit2 = (num // 10) % 10
        digit3 = num % 10
        if num == digit1 ** 3 + digit2 ** 3 + digit3 ** 3:
            print(num)

ArmstrongNum3()