#【实验1】函数递归。斐波那契数列（Fibonacci sequence），

def Fib(n):   #定义递归函数实现
    if n == 1 or n == 2:
        return 1
    return Fib(n-1)+Fib(n-2)

num = int(input("请输入要求的数："))
print(f"该数的Fibonacci数列为:{Fib(num)}")
# print("该数列的Fibonacci数列为："+str(Fib(num)))