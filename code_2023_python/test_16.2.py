class Student:
    def __init__(self, name, age, scores):
        self.name = name
        self.age = age
        self.scores = scores

    def get_name(self):
        return self.name

    def get_age(self):
        return self.age

    def get_score(self):
        return max(self.scores)

# 建立一个实例对象
zm = Student('zhangming', 20, [69, 88, 100])
print(zm.get_name(), zm.get_age(), zm.get_score())

##建立一个实例对象，
Student_score = Student("张三",20,[69,88,100])
print(Student_score.get_name())
print(Student_score.get_age())
print(Student_score.get_score())