#[实验1] 用 input输入两个值，求和并用print函数格式化输出
print('实验1')
a = int (input("第一个数："))
b = int (input("第二个数："))
c = a + b
print(f"第一个数是{a},第二个数是{b},和为{c}。")


#[实验2] 打印九九乘法表
print('\n实验2')
for i in range(1,10):
    print('\n')
    for j in range(1,i+1):
        print(f'{i}*{j} = {i*j}',end = '  ')
print()


#[实验3]近似计算圆周率
print('\n实验3')
import random,math
s = 1 * 1
c = int(input("投掷飞镖次数："))
hit = c
hits = 0
while hit != 0:
    hit = hit - 1
    x = random.uniform(-1,1)
    y = random.uniform(-1,1)
    z = math.sqrt(x**2+y**2)
    if z <= 1:  #如果在单位圆中
        hits = hits + 1     #命中
PI = (hits/c)*4
print(PI)


#[实验4]
print('\n实验4')
str = '1234abcdc'
print(str)  #输出字符串
print(str[0:-1])   #输出第一个到倒数第二个所有字符
print(str[0])#输出字符串第一个字符
print(str[2:5])#输出从第三个开始到第五个的字符
print(str[2:])#输出从第三个开始后的所有字符
print(str[1:5:2])#输出从第二个开始到第五个并且每隔一个的字符（步长为2）
print(str,str)#输出字符串两次
print('-----------------------------------')
print('hello\nrunoob')
print(r'hello\nrunoob') #使用r来避免转义字符


#[实验5] 输入字符串http://sss.sina.com.cn，输出以下内容
print('\n实验5')
s = 'http://sss.sina.com.cn'
print(s.count('s')) #(1)字符串中字母s出现的次数
print(s.find('sina'))    #(2)字符串中“sina”子字符串出现的位置
print(s.replace('.','*'))    #(3)将字符串中所有的.替换成*
print(s[s.find('sina'):s.find('sina')+4])   #(4)sina子字符，正向切片
print(s[s.find('sina')+3:s.find('sina')-1:-1])#(4)sina子字符，反向切片
print(s.capitalize())  #(5)首字母大写
print(len(s))   #(6)字符串的总字符个数


#[实验6]输入3门课a,b,c。求三门成绩总分、平均值（整数，四舍五入），最高和最低值。
#权重0.5-0.3-0.2计入总成绩，求总评成绩（先求和再四舍五入）
print('\n实验6')
from decimal import Decimal  # 引用decimal模块中的Decimal方法
a = input("a:")
b = input("b:")
c = input("c:")
sum = float(a) + float(b) + float(c)
print(f"总成绩：{sum}")
b2 = Decimal(b).quantize(Decimal("1."), rounding = "ROUND_HALF_UP")
print(f'平均分：{Decimal(sum/3).quantize(Decimal("1."), rounding = "ROUND_HALF_UP")}') #round
a = float(a); b = float(b); c = float(c)
print(f'最高值：{max(a,b,c)}')
print(f'最低值：{min(a,b,c)}')
sum = a*0.5 + b*0.3 + c*0.2
print(f'总评成绩：{Decimal(sum).quantize(Decimal("1."), rounding = "ROUND_HALF_UP")}')

#[实验7] 输入一个三位数的整数，求其各位数字之和及乘积
print('\n实验7')
x = input("请输入三位整数：")
sum = int(x[0]) + int(x[1]) + int(x[2])
print(f"和：{x[0]}+{x[1]}+{x[2]}={sum}")
s = int(x[0]) * int(x[1]) * int(x[2])
print(f"积：{x[0]}*{x[1]}*{x[2]}={s}")
