def isPrime(n):
    if n < 2:
        return False
    for i in range(2,int(n**0.5)+1):
        if n % i == 0:
            return False
    return True
num = int(input('请输入一个整数：'))
if isPrime(num):
    print(f"{num}是素数。")
else:
    print(f"{num}不是素数。")