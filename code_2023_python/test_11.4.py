#4. 定义一个函数is_prime(n)，
# 判断输入的n是不是素数，是的话返回True，否则返回False。
# 通过键盘输入两个整数X和Y，调用此函数输出两数范围之内素数的个数（包括X和Y）。
def is_prime(n):
    if n<=1:
        return False
    for j in range(2,int(n**0.5)+1):
        if n%j == 0:
            return False
    return True

X = int(input("请输入整数X:"))
Y = int(input("请输入整数Y:"))
num = 0
for i in range(min(X,Y),max(X,Y)+1):
    if is_prime(i):
        num = num + 1
print(f"{min(X,Y)}和{max(X,Y)}之间的素数有{num}个。")