#输入一字符串，求出字符串中有多少种字符，以及每个字符的个数。
def charCount(s):
    record = dict()
    for k in s:
        if k not in record:
            record[k] = 1
        else:
            record[k] = record[k] + 1
    for key in record:
        print(f"{key}:{record[key]}")
string = input("输入：")
charCount(string)