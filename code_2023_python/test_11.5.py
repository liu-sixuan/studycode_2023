# 5.一个数如果从左往右读和从右往左读数字是相同的，则称这个数是回文数，
# 如121，1221，15651都是回文数。
# 现在请写出一个函数h（n），判断n是否为回文数，是的话返回True，否则返回False。
# 再利用上面的判断素数函数，找出所有既是回文数又是素数的3位十进制数。
def h(n):
    # 将数字转换为字符串
    num_str = str(n)
    # 使用切片操作[::-1]将字符串逆序
    reversed_str = num_str[::-1]
    # 判断逆序字符串和原字符串是否相等
    if num_str == reversed_str:
        return True
    else:
        return False

def is_prime(n):
    if n<=1:
        return False
    for j in range(2,int(n**0.5)+1):
        if n%j == 0:
            return False
    return True
#找出所有既是回文数又是素数的3位十进制数。
for i in range(100,1000):
    if is_prime(i) and h(i):
        print(i,end = ' ')