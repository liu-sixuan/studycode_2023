#回文数问题。一个数是不是回文数指的是这个数是不是对称，
# 例如12321是回文数，因为个位与万位相同，十位与千位相同。
#解题思路：将数字转换为字符串，然后通过取字符比较。
def huiWen(string):
    for i in range(len(string)):
        if(string[i] != string[len(string)- 1 - i] ):
            return False
    return True

s = input("请输入一个数：")
if(huiWen(s) == True):
    print("是回文数")
else:
    print("不是回文数")