# 4.用random产生0 - 100的随机整数，然后通过键盘输入猜测的数，猜对则输出“猜对了”，
# 猜错则输出“小了，请继续”或“大了，请继续”，直到猜对为止。

import random

def GuessNum():
    # 生成随机数
    random_num = random.randint(0, 100)
    print("猜数字游戏开始")
    # 循环猜测
    while True:
        guess = int(input("请输入你猜测的数："))
        if guess == random_num:
            print("猜对了！")
            break
        elif guess < random_num:
            print("小了，请继续")
        else:
            print("大了，请继续")

GuessNum()
