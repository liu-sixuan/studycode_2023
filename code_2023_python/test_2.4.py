#19221108-计科211-刘斯旋
#[实验11]设计和实现聪明的尼姆游戏（人机对战）

from math import log2
from random import randint,choice

#这个函数写计算机的回合中，计算机取多少个的决策
def everyStep(n):
    half = n/2  #最大能取的
    m = 1   #最小能取的
    #所有可能满足条件的取法
    possible = []
    while True:
        rest = 2**m - 1 #满足聪明尼姆决策的最小剩余值
        if rest>= n:    #最小剩余值比整个剩余的值还大，不满足条件，说明遍历完了
            break
        if rest >= half:    #此时至少可以有一个满足条件的取法了。
            possible.append(n-rest)     # n-rest 意思是取走n-rest个物体，剩下满足条件的rest值
        m += 1  #试探下一种
    #如果至少存在一种取法使得剩余物品数量为2^n -1
    if possible:
        return choice(possible)
    #无法使剩余物品数量为2^（n-1)，随机取走一些
    return randint(1,int(half))     #最少取走一个，最多一半

def smartNimuGame(n):
    while n>1:  #还没走完
        #人类玩家先走
        print(f"It's your turn,and we have {n} left.")
        #确保人类玩家输入合法的整数值
        while True:
            try:
                num = int(input('How many do you want to take:'))
                assert 1 <= num <= n//2     #检查输入的数是否合法
                break
            except:
                print(f'Must be between 1 and {n//2}.')     #必须在1到半数之间
        n -= num    #人类拿走了num个
        if n == 1:
            return  'I fail.'       #剩一个，计算机输了
        #计算机拿走一些
        n -= everyStep(n)
    else:      #这个else和while配合使用，意思是跳出while循环后要执行的语句。其实可以去掉。
        return 'You fail.'

print(smartNimuGame(randint(1,100)))
