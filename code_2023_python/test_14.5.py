def fun(n):
    #非递归
    # sum = 0
    # for i in range(1,n+1):
    #     sum += i/(2*i+1)
    # return sum
    if n>0:
        return n/(2*n + 1)+fun(n-1)
    else:
        return 0

print(fun(1))
print(fun(2))
print(fun(10))
print(fun(100))
print(fun(500))