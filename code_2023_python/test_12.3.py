#用 jieba 分词，计算字符串 s 中的中文词汇个数，不包括中文标点符号。显示输出分词后的结果，用”/ ”分隔，以及中文词汇个数。示例如下：
# 输入：对于这么优秀的电影来说，再多的赞美都是多余的。
# 输出：对于/ 这么/ 优秀/ 的/ 电影/ 来说/ 再/ 多/ 的/ 赞美/ 都/ 是/ 多余/ 的/
# 中文词语数是：14

import jieba
#s存储要进行分词的中文文本
s = '对于这么优秀的电影来说，再多的赞美都是多余的。'
#使用字符串的 replace 方法删除了字符串 s 中的一些中文标点符号，将其保存为新的字符串。
#过滤常用的标点符号
s = s.replace('，','').replace('。','').replace('、','').replace('“','').replace('”','')
#jieba.cut 方法对去除标点符号后的新字符串 s 进行分词。
# 该方法返回一个 generator，我们将其转换为列表 word_list。
words = jieba.cut(s)
word_list = list(words)

# 去除所有中文标点符号
punctuation = ['。', '，', '、', '“', '”', '！', '？', '：', '；']
#word_list保存非标点符号的词汇
word_list = [word for word in word_list if word not in punctuation]

#使用 '/' 作为连接符将 word_list 列表中的词汇连接起来形成一个新的字符串 result
result = '/ '.join(word_list)
word_count = len(word_list)

print(result)
print("中文词语数是:", word_count)