#约瑟夫环问题

#方法一 使用标准库itertools
from itertools import cycle

def demo1(lst,k):
    t_lst = lst[:]

    #游戏进行到只剩最后一个人为止
    while len(t_lst) > 1:
        print(t_lst)    #打印每轮的排位情况。
        #创建 cycle 对象
        # cycle()会把传入的一个序列无限重复下去
        #count()会创建一个无限的迭代器，所以上述代码会打印出自然数序列，根本停不下来，只能按Ctrl+C退出。
        #repeat()负责把一个元素无限重复下去，不过如果提供第二个参数就可以限定重复次数.
        c = cycle(t_lst)    #把人围成一个圈
        #通过takewhile()等函数根据条件判断来截取出一个有限的序列：
        # natuals = itertools.count(1)
        # ns = itertools.takewhile(lambda x: x <= 10, natuals)
        #next() 返回迭代器的下一个项目。
        # next() 函数要和生成迭代器的 iter() 函数一起使用。
        #从1-k报数
        for i in range(k):  #开始报数，1-》k。
            t = next(c)     #next是从第一个值开始的。
        #循环结束的时候t正好是数到k的那个人
        #报到 k 的人出局。
        index = t_lst.index(t)
        #注意这里“加法”的顺序！得把k后面的放在前面（按顺序）
        t_lst = t_lst[index+1:] + t_lst[:index] #更新人群。把k前后的列表相加。

    #循环结束，只剩下最后一个人
    return t_lst[0]

#方法二 使用列表方法
def demo2(lst,k):
    t_lst = lst[:]  #python的列表可以直接赋值，但是直接赋值指向的是同一个地址，互相影响。
    #游戏进行到只剩最后一个人为止
    while len(t_lst) > 1:
        print(t_lst)
        #模拟围成一个圈循环报数
        #把头尾相接。
        for i in range(k-1):    #每报一个数就把这个数挪到列表的最后面去
            t_lst.append(t_lst.pop(0))
        t_lst.pop(0)    #第k个直接删除
    #游戏结束
    return t_lst[0]

n =int(input('请输入初始人数 n:'))
k =int(input('请输入报数临界值 k:'))
lst = list(range(1,n+1))
print(demo1(lst,k))
print(demo2(lst,k))