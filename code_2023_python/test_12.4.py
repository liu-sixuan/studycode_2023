#《天龙八部》是小说作品的网络版本，文件名为“天龙八部-网络版.txt”。（见附件）
#问题1：请编写程序，对这个文本中出现的字符进行统计，
# 字符与出现次数之间用冒号:分隔，
# 将前 100 个最常用字符分别输出保存到“天龙八部-字符统计.txt”中。

#注：统计字符时，我去掉了标点符号
import re
from collections import Counter

# 读取文本文件
with open('天龙八部-网络版.txt', 'r', encoding='utf-8') as f:
    text = f.read()

# 使用正则表达式将标点符号去除
text = re.sub(r'[^\w\s]', '', text)

# 使用正则表达式提取非空白字符
text = re.findall(r'\S', text)

# 统计字符出现次数
char_count = Counter(text)

# 获取前100个最常用字符及其出现次数
top_100_chars = char_count.most_common(100)

# 将结果保存到文件
with open('天龙八部-字符统计.txt', 'w', encoding='utf-8') as f:
    for char, count in top_100_chars:
        line = f"{char}:{count}\n"
        f.write(line)