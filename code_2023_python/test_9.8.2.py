# 4.编写程序，用户输入一段英文，然后输出这段英文中所有长度为3个字母的单词
# （思路1：）用s.split()分割字符串，得到列表，len（）判断长度，对每个列表元素进行分析。、、
import string
import re
def Words3(s):
    words = re.split(r'[\s\W]+', s) #以空格和标点符号分割
    s1 = []
    for word in words:
        if len(word) == 3:
            s1.append(word)
    for word3 in s1:
        print(word3,end = ' ')

#s = input("请输入一段英文：")
test = "I am the King OF the world.And I will never be a big pig."
Words3(test)
