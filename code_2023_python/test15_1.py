import random
from collections import Counter

# 创建并写入随机整数到 data.txt
with open("data.txt", "w") as file:
    #写1000行。可以随意改变
    for _ in range(1000):
        num = random.randint(1, 100)
        file.write(str(num) + "\n")

# 读取 data.txt 并统计数字出现次数
with open("data.txt", "r") as file:
    numbers = [int(line.strip()) for line in file.readlines()]
    counter = Counter(numbers)

# 找出出现次数最多的前5个数字
most_common = counter.most_common(5)

# 写入结果到 most.txt
with open("most.txt", "w") as file:
    for num, count in most_common:
        file.write(f"{num} 出现: {count}次\n")
