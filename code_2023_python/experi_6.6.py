# 6.输入有效位数, 按照下列公式计算圆周率π的有效值，精确到特定位。

from decimal import *
from math import sqrt

getcontext().prec = int(input("请输入pi的计算精度，有效位数为："))

def compute_pi():
    pi = Decimal(2.0)
    denominator = Decimal(2.0)
    diff = 1

    while diff >= 10**(-getcontext().prec):
        new_pi = pi * Decimal(2) / Decimal(sqrt(float(denominator)))
        diff = abs(new_pi - pi)
        pi = new_pi
        denominator = 2 + Decimal(sqrt(float(denominator)))

    return pi

pi = compute_pi()
print(f"精确到小数点后{getcontext().prec-1}位的PI值为：{pi:.{getcontext().prec-1}f}")
