#include<stdio.h>
#define _CRT_SECURE_NO_WARNINGS 1
//scanf函数返回的是：实际读到的数据的个数
//scanf 函数读取失败的时候，返回的是EOF
//EOF - end of file 文件结束标志
//int main() {

//写法一
//    int iq = 0;
//    //输入
//    while (scanf("%d", &iq) == 1) {
//        //输出
//        if (iq >= 140)
//            printf("Genius\n");
//    }
//
//    return 0;
//}
//

////写法二
//int main() {
//    int iq = 0;
//    //输入
//    while (scanf("%d", &iq) != EOF) {
//        //输出
//        if (iq >= 140)
//            printf("Genius\n");
//    }
//
//    return 0;
//}


 //  / 除号的两端如果都是整数，执行的是整数除法
 //  / 除号的两端至少有一个数是浮点数，才能执行浮点数的除法

 // 除号
 //% 取模（取余）,%操作的两个操作数必须都是整数

//int main()
//{
//	int month = 1;
//	printf("month=%02d\n", month);//输出不够2位时，左边拿0填充
//	return 0;
//}

int main()
{
    int ret = printf("Hello world!");
    printf("\n");
    printf("%d\n", ret);
    return 0;
}
