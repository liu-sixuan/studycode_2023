#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
//#define 定义符号 / 宏
//#define M 100
//
//int Max(int x, int y)
//{
//	//if (x > y)
//	//	return x;
//	//else
//	//	return y;
//	return (x > y ? x : y);
//}
//
////定义宏 
//#define MAX(x,y)  (x>y?x:y) //(x>y?x:y)是宏体
//#define ADD(x,y)  ((x)+(y))
//
//int main()
//{
//	//int a = M;
//	//printf("%d\n", M);
//	//printf("%d\n", a);
//
//	int a = 10;
//	int b = 20;
//	//int m = Max(a, b);
//	int m = MAX(a, b);
//	//int m = (a > b ? a : b);
//	printf("%d\n", m);
//
//	return 0;
//}
//******************************************************************************************************
//多组输入练习
//int main() 
//{
//	int num = 0;
//	while (scanf("%d", &num) == 1) //只要输入不停止就继续循环
//	{
//		for (int i = 0; i < num; i++)
//		{
//			printf("*");
//		}
//		printf("\n");
//	}
//	return 0;
//}
//******************************************************************************************************
//结构体练习
//
////创建结构体
//struct Stu
//{
//	char name[20];//名字
//	int age;	//年龄
//	char sex[5]; // 性别
//	char id[15]; //学号
//};
//
//void print(struct Stu* ps)
//{
//    //printf("%s %s %d\n", (*ps).name, (*ps).sex, (*ps).age);
//    //->
//    //结构体的指针->成员名
//	printf("%s %d %s %s\n", ps->name, ps->age, ps->sex,ps->id);
//}
//
//int main()
//{
//	//结构体的初始化
//	struct Stu s = {
//		"L慕联",20,"女","19221108"
//	};
//	printf("%s %d %s %s\n",s.name,s.age,s.sex,s.id);
//	print(&s);
//	return 0;
//}
//******************************************************************************************************
//适当的使用{}可以使代码的逻辑更加清楚。
//代码风格很重要
//int main()
//{
//	int a = 0;
//	int b = 2;
//	if (a == 1)
//		if (b == 2)
//			printf("hehe\n");
////-------------------------------
//	else                            // 注意看，此时else与离他最近的if 匹配！
//		printf("haha\n");			//	这里else悬空，是种很不好的代码风格。
////-------------------------------
//	return 0;
//}
//******************************************************************************************************
//if 书写风格的对比
//代码1 这种代码虽然简洁，但是不容易看。
//if (condition) {
//	return x;
//}
//return y;
//代码2 这种代码看似复杂，实则易于阅读。上选。
//if (condition)
//{
//	return x;
//}
//else
//{
//	return y;
//}
//******************************************************************************************************
//在写判断条件的时候，把常量当成左值，不容易出错把 == 写成=。
////代码3
//int num = 1;
//if (num == 5)
//{
//	printf("hehe\n");
//}
////代码4
//int num = 1;
//if (5 == num)		//这种的最好。
//{
//	printf("hehe\n");
//}
//******************************************************************************************************
//switch语句的练习
int main()
{
	int day = 0;
	switch (day)	//switch 后面必须跟 整型表达式
	{
	case 1:		//case后面必须是 整形常量表达式
		printf("星期一\n");
		break;
	case 2:
		printf("星期二\n");
		break;
	case 3:
		printf("星期三\n");
		break;
	case 4:
		printf("星期四\n");
		break;
	case 5:
		printf("星期五\n");
		break;
	case 6:
		printf("星期六\n");
		break;
	case 7:
		printf("星期天\n");
		break;
	default:	//如果所有情况都不满足，自动进入default语句。default语句可以放在case能放的任意位置。
		printf("输入错误！\n");
		break;
	}
	return 0;
}