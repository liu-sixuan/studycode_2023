#include<stdio.h>
int a = 1;
int main() {
	printf("%d", a);
	return 0;
}
//a = 100;   //不可以对全局变量进行赋值操作，只能初始化。
//int a = 1;  //使用变量之前必须先声明，可以不给值。但是必须在之后定义