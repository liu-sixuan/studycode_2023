#define _CRT_SECURE_NO_WARNINGS 1
#include<stdio.h>
/*1.我想看看getchar是否是把回车也当做字符读取了的。	结果表明是这样的。*/
//int main()
//{
//	int i = 1; char ch = 0;
//	while ((ch = getchar()) != EOF)
//	{
//		
//		printf("%d %c\n",i++,ch);
//	}
//	return 0;
//}

/*2.我想看下scanf 会在什么情况下停止读字符。*/
//scanf 碰到空格就停止读入
//%[^\n]  ——让scanf 读取空格和其他空白字符，直到达到输入行的结尾。（不包括 \n）
//% [^ \n] % *c  —— 包括 \n
//int main()
//{
//	char test[10]={0};
//	scanf("%[^\n]%*c", test);	//	
//	int i = 0;
//	while (i < 10)
//	{
//		printf("%c", test[i++]);
//	}
//	/*错误代码*/
//	//while (char ch = getchar()!= EOF)
//	//{
//	//	printf("%c", ch);
//	//}
//	return 0;
//}

/*3.写for 循环的建议：建议写成左闭右开，如 for (int i = 0;i < 10;i++)*/

/*4.计算1！+...+10！*/
int main()
{
	int sum = 0; int mul = 1;
	for (int i = 1; i < 11; i++)
	{
		mul *= i;
		sum += mul;
	}
	printf("%d\n", sum);
	return 0;
}